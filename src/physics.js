var CollisionDetector = cc.Node.extend({
    laser : null,
    meteor : null,
    bulletAndStab : null,
    player : null,
    ctor : function(player,obstacleLayer){
        this._super();
        this.SetPlayer(player);
        this.bulletAndStab = obstacleLayer.GetBulletAndStab();
        this.laser = obstacleLayer.GetLaser();
        this.meteor = obstacleLayer.GetMeteor();
    },
    SetPlayer : function(player){
        this.player= player;
    },
    CheckCollision : function(target){
        this.CheckBulletAndStab(target);
        this.CheckLaser(target);
        this.CheckMeteor(target);
    },
    CheckBulletAndStab : function(target){
        for(var i = 0;i < this.bulletAndStab.length;i++){
            var obstacle = this.bulletAndStab[i];
            var positionDeltaX = Math.abs(this.player.x - obstacle.sprite.x);
            var positionDeltaY = Math.abs(this.player.y - obstacle.sprite.y);
            if(obstacle.type == ObstacleType.Bullet){
                switch (obstacle.direction){
                    case 1 :
                        if((positionDeltaX <= BULLET_WIDTH / 2 + PLAYER_SCALE / 2) && (positionDeltaY <= BULLET_LENGTH / 2))
                        {target.PlayerDie();obstacle.removeFromParent(true);return;}
                        else if(positionDeltaX <= REWARD_BULLET_LENGTH && positionDeltaY <= REWARD_BULLET_WIDTH && !obstacle.isRewarded){
                            target.PlayerReward(1,positionDeltaX);
                            obstacle.isRewarded = true;
                        }
                        break;
                    case 3 :
                        if((positionDeltaX <= BULLET_WIDTH / 2 + PLAYER_SCALE / 2) && (positionDeltaY <= BULLET_LENGTH / 2))
                        {target.PlayerDie();obstacle.removeFromParent(true);return;}
                        else if(positionDeltaX <= REWARD_BULLET_LENGTH && positionDeltaY <= REWARD_BULLET_WIDTH && !obstacle.isRewarded){
                            target.PlayerReward(1,positionDeltaX);
                            obstacle.isRewarded = true;
                        }
                        break;
                    case 2 :
                        if((positionDeltaY <= BULLET_WIDTH / 2 + PLAYER_SCALE / 2) && (positionDeltaX <= BULLET_LENGTH / 2))
                        {target.PlayerDie();obstacle.removeFromParent(true);return;}
                        else if(positionDeltaY <= REWARD_BULLET_LENGTH && positionDeltaX <= REWARD_BULLET_WIDTH && !obstacle.isRewarded){
                            target.PlayerReward(1,positionDeltaY);
                            obstacle.isRewarded = true;
                        }
                        break;
                    case 4 :
                        if((positionDeltaY <= BULLET_WIDTH / 2 + PLAYER_SCALE / 2) && (positionDeltaX <= BULLET_LENGTH / 2))
                        {target.PlayerDie();obstacle.removeFromParent(true);return;}
                        else if(positionDeltaY <= REWARD_BULLET_LENGTH && positionDeltaX <= REWARD_BULLET_WIDTH && !obstacle.isRewarded){
                            target.PlayerReward(1,positionDeltaY);
                            obstacle.isRewarded = true;
                        }
                        break;
                    default : break;
                }
            }
            else if(obstacle.type == ObstacleType.GroundStab){
                if(positionDeltaX <= GROUND_STAB_HALF && positionDeltaY <= GROUND_STAB_HALF)
                {target.PlayerDie();return;}
                else if(positionDeltaX <= REWARD_GROUNDSTAB && positionDeltaY <= REWARD_GROUNDSTAB && !obstacle.isRewarded){
                    target.PlayerReward(2,positionDeltaX);
                    obstacle.isRewarded = true;
                }
            }
        }
    },
    CheckLaser : function(target){
        for(var i = 0;i < this.laser.length;i++){
            var obstacle = this.laser[i];
            var positionDeltaX = Math.abs(this.player.x - obstacle.sprite.x);
            var positionDeltaY = Math.abs(this.player.y - obstacle.sprite.y);
            switch (obstacle.direction){
                case 1 :
                    if(positionDeltaX <= LASER_HALF_LENGTH + PLAYER_SCALE / 2)
                    {target.PlayerDie();return;}
                    else if(positionDeltaX <= REWARD_LASER && !obstacle.isRewarded){
                        target.PlayerReward(3,positionDeltaX);
                        obstacle.isRewarded = true;
                    }
                    break;
                case 2 :
                    if(positionDeltaY <= LASER_HALF_LENGTH + PLAYER_SCALE / 2)
                    {target.PlayerDie();return;}
                    else if(positionDeltaY <= REWARD_LASER && !obstacle.isRewarded){
                        target.PlayerReward(3,positionDeltaY);
                        obstacle.isRewarded = true;
                    }
                    break;
                case 3 :
                    if(positionDeltaX <= LASER_HALF_LENGTH + PLAYER_SCALE / 2)
                    {target.PlayerDie();return;}
                    else if(positionDeltaX <= REWARD_LASER && !obstacle.isRewarded){
                        target.PlayerReward(3,positionDeltaX);
                        obstacle.isRewarded = true;
                    }
                    break;
                case 4 :
                    if(positionDeltaY <= LASER_HALF_LENGTH + PLAYER_SCALE / 2)
                    {target.PlayerDie();return;}
                    else if(positionDeltaY <= REWARD_LASER && !obstacle.isRewarded){
                        target.PlayerReward(3,positionDeltaY);
                        obstacle.isRewarded = true;
                    }
                    break;
                default : break;
            }
        }
    },
    CheckMeteor : function(target){
        for(var i = 0;i < this.meteor.length;i++){
            var obstacle = this.meteor[i];
            var positionDeltaX = Math.abs(this.player.x - obstacle.sprite.x);
            var positionDeltaY = Math.abs(this.player.y - obstacle.sprite.y);
            if(positionDeltaX <= PLAYER_SCALE && positionDeltaY <= PLAYER_SCALE)
                target.PlayerDie();
            else if(positionDeltaX <= 80 && positionDeltaY <= 80 && !obstacle.isRewarded){
                target.PlayerReward(4,Math.min(positionDeltaX,positionDeltaY));
                obstacle.isRewarded = true
            }
        }
    },
    CheckGetReward : function(reward,player){
        var positionDeltaX = Math.abs(reward.sprite.x - player.x);
        var positionDeltaY = Math.abs(reward.sprite.y - player.y);
        if(positionDeltaX <= REWARD_COIN && positionDeltaY <= REWARD_COIN){
            reward.sprite.removeFromParent(true);
            reward.Instantiate();
            AudioManager(res.coinMp3);
            return true;
        }
        return false;
    }
});

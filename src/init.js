var Initializer = function(){
    this.InitAnimation = function(){
        AnimationCache = cc.spriteFrameCache;
        AnimationCache.addSpriteFrames(res.GroundStab_warningPlist,res.GroundStab_warningPng);
        AnimationCache.addSpriteFrames(res.GroundStab_operatingPlist,res.GroundStab_operatingPng);
        AnimationCache.addSpriteFrames(res.Laser_warningPlist,res.Laser_warningPng);
        AnimationCache.addSpriteFrames(res.Laser_OperatingPlist,res.Laser_OperatingPng);
        AnimationCache.addSpriteFrames(res.Meteorite_operatingPlist,res.Meteorite_operatingPng);
        AnimationCache.addSpriteFrames(res.Meteorite_warningPlist,res.Meteorite_warningPng);
        AnimationCache.addSpriteFrames(res.Coin_plist,res.Coin_png);
        AnimationCache.addSpriteFrames(res.UI_plist,res.UI_png);
        Animations.GroundStab_warning = null;
        Animations.GroundStab_operating = null;
        Animations.Laser_warning = null;
        Animations.Laser_operating = null;
        Animations.Meteor_warning = null;
        Animations.Meteor_operating = null;
        Animations.Player = null;
        var animFrames = [];
        this.GenerateFrames(animFrames,1,6,"warning");
        Animations.GroundStab_warning = animFrames.concat();
        animFrames = [];
        this.GenerateFrames(animFrames,7,1,"dici");
        this.GenerateFrames(animFrames,1,7,"dici");
        Animations.GroundStab_operating = animFrames.concat();
        animFrames = [];
        this.GenerateFrames(animFrames,1,7,"laser");
        Animations.Laser_warning = animFrames.concat();
        animFrames = [];
        this.GenerateFrames(animFrames,3,23,"lasereffect");
        Animations.Laser_operating = animFrames.concat();
        animFrames = [];
        this.GenerateFrames(animFrames,1,25,"meteorite");
        Animations.Meteor_warning = animFrames.concat();
        animFrames = [];
        this.GenerateFrames(animFrames,26,31,"meteorite");
        Animations.Meteor_operating = animFrames.concat();
        animFrames = [];
        this.GenerateFrames(animFrames,1,4,"player");
        Animations.Player = animFrames.concat();
        animFrames = [];
        this.GenerateFrames(animFrames,1,4,"coin");
        this.GenerateFrames(animFrames,3,1,"coin");
        Animations.Coin = animFrames.concat();
    };
    this.GenerateFrames = function(animFrames,begin,end,index){
        var alter = begin > end ? -1 : 1;
        for(var i = begin;begin > end ? (i > end) : (i < end);i += alter){
            var frame = AnimationCache.getSpriteFrame(index + i + ".png");
            animFrames.push(frame);
        }
    };
};

var AnimateController = function(animFrames,perUnit,isLoop){
    var animation = new cc.Animation(animFrames,true);
    animation.setDelayPerUnit(perUnit);
    animation.setRestoreOriginalFrame(isLoop);
    return animation;
};

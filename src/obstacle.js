var ObstacleLayer = cc.Layer.extend({
    currentTime : 0,
    obstacleWaveController : null,
    obstacleManager : null,
    ctor : function(){
        this._super();
        this.obstacleManager = new ObstacleManager();
        this.obstacleWaveController = new ObstacleWaveController();
        this.Initialize();
        this.addChild(this.obstacleWaveController);
        this.scheduleUpdate();
        Obstacle.prototype.manager = this.obstacleManager;
    },
    Initialize : function(){
        this.currentTime = 0;
        this.waveIterator = 0;
        this.currentWaveTime = 0;
        this.allowToGenerateWaves = true;
        this.obstacleManager.Initialize();
        this.obstacleWaveController.Initialize();
    },
    update : function(dt){
        this.currentTime += dt;
        this.obstacleManager.Update(dt);
        this.obstacleWaveController.Update(dt);
    },
    GetBulletAndStab : function(){
        return this.obstacleManager.bulletAndStab;
    },
    GetLaser : function(){
        return this.obstacleManager.laser;
    },
    GetMeteor : function(){
        return this.obstacleManager.meteor;
    },
    GameOver : function(){
        var that = this;
        this.obstacleWaveController.allowToGenerateWaves = false;
        this.runAction(cc.sequence(
            cc.delayTime(1),
            cc.callFunc(function(){
                that.obstacleWaveController.removeAllChildren(true);
            })
        ));
    }
});

var ObstacleWaveController = cc.Node.extend({
    currentWave : 0,
    currentDifficultyCoefficient : 0,
    waveIterator : 0,
    currentTime : 0,
    allowToGenerateWaves : true,
    nextWaveTime : 0,
    currentWaveTime : 0,
    obstacleBuilder : null,
    isRotate : false,
    currentTotDifficulty : 0,
    waveNum : 0,
    isInit : false,
    ctor : function(){
        this._super();
        this.Initialize();
        this.obstacleBuilder = new ObstacleBuilder();
    },
    init : function(){
        this.isInit = true;
        if(IS_TUTORED)
            return false;
        this.waveNum++;
        this.currentDifficultyCoefficient = 0;
        this.waveIterator = 0;
        this.currentWave = 0;
        this.nextWaveTime += waves[this.currentWave].waveTime;
        this.isRotate = Math.floor(Math.random() * 2) > 0;
        this.currentWaveTime = this.currentTime;
        return true;
    },
    Initialize : function(){
        var that = this;
        this.waveNum = 0;
        this.isInit = false;
        this.allowToGenerateWaves = false;
        this.runAction(cc.sequence(
            cc.delayTime(3),
            cc.callFunc(function(){
                that.allowToGenerateWaves = true;
            })
        ));
        this.currentTime = 0;
        this.currentWave = 0;
        this.currentDifficultyCoefficient = 0;
        this.waveIterator = 0;
        this.isRotate = false;
        this.nextWaveTime = 0;
        this.currentWaveTime = 0;
        this.currentDifficultyCoefficient = 0;
    },
    GenerateWave : function(wave){
        if(!this.allowToGenerateWaves)return;
        if(this.currentTime > this.nextWaveTime){
            this.SelectWave();
            return;
        }
        for(;this.waveIterator < wave.array.length;this.waveIterator++){
            var obstacle = wave.array[this.waveIterator];
            if(obstacle.time + this.currentWaveTime <= this.currentTime){
                this.Create(obstacle.type,obstacle.position,obstacle.duration,this.isRotate);
            }
            else break;
        }
    },
    SelectWave : function(){
        if(this.waveNum == 0 && !this.isInit){if(this.init())return;}
        if(this.waveNum == 1){
            IS_TUTORED = true;
        }
        this.waveNum++;
        this.waveIterator = 0;
        var pair = this.GetWaveIndexRange();
        this.currentWave = parseInt(Math.random() * (pair.y - pair.x + 1) + pair.x);
        this.nextWaveTime += waves[this.currentWave].waveTime;
        this.isRotate = Math.floor(Math.random() * 2) > 0;
        this.currentWaveTime = this.currentTime;
    },
    Update : function(dt){
        if(!this.allowToGenerateWaves)return;
        this.currentTime += dt;
        this.GenerateWave(waves[this.currentWave]);
    },
    GetWaveIndexRange : function(){
        if(this.waveNum == 1){
            this.currentDifficultyCoefficient += 1;
            return cc.p(1,6);
        }
        else if(this.waveNum == 2 || this.waveNum == 3){
            this.currentDifficultyCoefficient += 3;
            return cc.p(1,15);
        }
        else if(this.waveNum == 4){
            this.currentDifficultyCoefficient += 5;
            return cc.p(1,50);
        }
        else if(this.waveNum == 5 || this.waveNum == 6){
            this.currentDifficultyCoefficient += 5;
            return cc.p(7,50);
        }
        else{
            this.currentDifficultyCoefficient += 5;
            return cc.p(16,50);
        }
    },
    Create : function(type,position,duration,isRotate){
        var obstacleNode = this.obstacleBuilder.CreateObstacle(type,position,duration,this.currentTime,isRotate);
        this.addChild(obstacleNode);
    },
    GetCoefficient : function(){
        var num = (1 + this.currentDifficultyCoefficient / 2.5) * (Math.max(0,this.waveNum - 1) * 0.3) + 1;
        return num;
    }
});

var Obstacle = cc.Node.extend({
    sprite : null,
    spawnTime : 0,
    isRewarded : false,
    ctor : function(type,position,duration,spawnTime,isRotate){
        this._super();
        this.type = type;
        this.position = position;
        this.duration = duration;
        this.spawnTime = spawnTime;
        this.sprite = null;
        this.isRotate = isRotate;
        this.isRewarded = false;
    },
    Destroy : function(){
        this.sprite.x = this.sprite.y = -100;
        this.sprite.removeFromParent(true);
    },
    AddToManager : function() {
        this.manager.InsertObstacle(this.type,this);
    }
});

Obstacle.prototype.manager = null;

var ObstacleBuilder = function(){
    this.CreateObstacle = function(type,position,duration,currentTime,isRotate){
        var ret = null;
        switch (type){
            case ObstacleType.Bullet :
                ret = new Bullet(position,duration <= 2.3 ? 2.3 : duration,currentTime,isRotate);
                break;
            case ObstacleType.Laser :
                ret = new Laser(position,duration,currentTime + 1.6,isRotate);
                break;
            case ObstacleType.GroundStab :
                ret = new GroundStab(position,1.0,currentTime + 1.5,isRotate);
                break;
            case ObstacleType.Meteor :
                ret = new Meteor(position,0.5,currentTime + 1,isRotate);
                break;
            case ObstacleType.Notice :
                ret = new Notice(position);
                break;
            default : break;
        }
        return ret;
    }
};

var Bullet = Obstacle.extend({
    distance: 0,
    direction: 0,
    rotation1 : 0,
    ctor: function (position, duration,spawnTime,isRotate) {
        this._super(ObstacleType.Bullet, position, duration,spawnTime,isRotate);
        this.sprite = new cc.Sprite(res.Bullet_png);
        this.addChild(this.sprite);
        this.GetParameters(position);
        this.sprite.attr({
            x: BulletPos[this.position].x,
            y: BulletPos[this.position].y,
            anchorX : 0.5,
            anchorY : 0.5,
            rotation : this.rotation1
        });
        this.sprite.scaleX = BULLET_LENGTH / this.sprite._contentSize.width;
        this.sprite.scaleY = BULLET_WIDTH / this.sprite._contentSize.height;
        this.Instantiate();
    },
    GetParameters: function (position) {
        var WINDOW_SIZE = cc.director.getWinSize();
        this.direction = Math.floor(position / 4) + 1;
        if(this.isRotate)
        {
            this.direction++;
            if(this.direction > 4)this.direction = 1;
            this.position += 4;
            if(this.position > 15)this.position -= 16;
        }
        this.distance = this.direction % 2 == 1? WINDOW_SIZE.height * 1.2 : WINDOW_SIZE.width * 1.2;
        this.rotation1 = 90 * this.direction;
    },
    Instantiate: function () {
        var xx, yy;
        var that = this;
        xx = this.direction % 2 == 0 ? this.distance : 0;
        yy = this.direction % 2 == 0 ? 0 : this.distance;
        if(this.direction == 2 || this.direction == 1){
            xx = -xx;
            yy = -yy;
        }
        var ac1 = cc.moveBy(this.duration, cc.p(xx, yy));
        var acf = cc.callFunc(function () {
            that.Destroy();
        });
        var seq = cc.sequence(ac1, acf);
        this.duration += 4;
        this.sprite.runAction(cc.sequence(seq));
        this.AddToManager();
    }
});

var Laser = Obstacle.extend({
    distance: 0,
    direction: 0,
    rotation1 : 0,
    laserEffect : null,
    ctor: function (position, duration,spawnTime,isRotate) {
        this._super(ObstacleType.Laser, position, duration,spawnTime,isRotate);
        this.sprite = new cc.Sprite("#laser1.png");
        this.laserEffect = new cc.Sprite("#lasereffect3.png");
        this.addChild(this.sprite);
        this.GetParameters(position);
        this.sprite.attr({
            x: BulletPos[this.position].x,
            y: BulletPos[this.position].y,
            anchorX : 0.5,
            anchorY : 0.5,
            rotation : this.rotation1
        });
        this.laserEffect.attr({
            x : LaserPos[this.position].x,
            y : LaserPos[this.position].y,
            anchorX : 1.05,
            anchorY : 0.5,
            rotation : this.rotation1
        });
        if(!ENVIRONMENT)
        {
            this.sprite.setScale(1.5);
            this.laserEffect.setScale(1.5);
        }
        this.Instantiate();
    },
    GetParameters: function (position) {
        this.direction = Math.floor(position / 4) + 1;
        if(this.isRotate)
        {
            this.direction++;
            if(this.direction > 4)this.direction = 1;
            this.position += 4;
            if(this.position > 15)this.position -= 16;
        }
        this.rotation1 = 90 * this.direction + 180;
    },
    Instantiate: function () {
        var that = this;
        this.sprite.runAction(
            cc.sequence(
                cc.moveTo(1.0,LaserPos[this.position]),
                cc.animate(AnimateController(Animations.Laser_warning,0.125,false)),
                cc.callFunc(function(){
                    that.StartOperating();
                })
            )
        );
    },
    StartOperating : function(){
        var that = this;
        AudioManager(res.laserMp3);
        this.addChild(this.laserEffect);
        this.AddToManager();
        this.laserEffect.runAction(cc.sequence(
            cc.animate(AnimateController(Animations.Laser_operating,this.duration / 22,false)),
            cc.callFunc(function(){that.laserEffect.removeFromParent(true);}),
            cc.callFunc(function(){
                that.sprite.runAction(cc.sequence(
                    cc.fadeOut(0.5),
                    cc.callFunc(function(){that.Destroy();})
                ))
            })
        )
        );
    }
});

var GroundStab = Obstacle.extend({
    waitTime: 0,
    sprite : null,
    ctor: function (position, duration,spawnTime,isRotate) {
        this._super(ObstacleType.GroundStab, position, duration,spawnTime,isRotate);
        this.sprite = new cc.Sprite('#warning1.png');
        this.sprite.attr({
            x : StabPos[isRotate ? ((3 - parseInt(position / 4)) + (position % 4 * 4)) : position].x,
            y : StabPos[isRotate ? ((3 - parseInt(position / 4)) + (position % 4 * 4)) : position].y,
            anchorX : 0.5,
            anchorY : 0.5
        });
        if(!ENVIRONMENT)
            this.sprite.setScale(1.5);
        this.Instantiate();
    },
    Instantiate: function () {
        var that = this;
        this.addChild(this.sprite);
        this.sprite.runAction(cc.sequence(cc.animate(AnimateController(Animations.GroundStab_warning,1.5 / 6,false)),cc.callFunc(function(){that.StartOperating();})));
    },
    StartOperating: function () {
        var that = this;
        this.AddToManager();
        this.sprite.runAction(cc.sequence(cc.animate(AnimateController(Animations.GroundStab_operating,1.5 / 14,false)),cc.callFunc(function(){that.Destroy();})));
    }
});

var Meteor = Obstacle.extend({
    waitTime: 0,
    sprite : null,
    ctor: function (position, duration,spawnTime,isRotate) {
        this._super(ObstacleType.Meteor, position, duration,spawnTime,isRotate);
        this.sprite = new cc.Sprite('#meteorite1.png');
        this.sprite.attr({
            x : MeteorPos[isRotate ? (2 - parseInt(position / 3) + (position % 3 * 3)) : position].x,
            y : MeteorPos[isRotate ? (2 - parseInt(position / 3) + (position % 3 * 3)) : position].y,
            anchorX : 0.5,
            anchorY : 0.5
        });
        if(!ENVIRONMENT)
            this.sprite.setScale(1.5);
        this.Instantiate();
    },
    Instantiate: function () {
        var that = this;
        this.addChild(this.sprite);
        this.sprite.runAction(cc.sequence(cc.animate(AnimateController(Animations.Meteor_warning,1 / 19,false)),cc.callFunc(function(){that.StartOperating();})));
    },
    StartOperating: function () {
        var that = this;
        AudioManager(res.meteMp3);
        this.AddToManager();
        this.sprite.runAction(cc.sequence(cc.callFunc(function(){ParticleManager(that,cc.p(that.sprite.x,that.sprite.y),1,res.FX4)}),cc.animate(AnimateController(Animations.Meteor_operating,2 / 14,false)),cc.callFunc(function(){that.Destroy();})));
    }
});

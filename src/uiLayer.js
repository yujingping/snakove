var GamingSceneUILayer = cc.Layer.extend({
    gamingSceneUI : null,
    gameOverUI : null,
    ctor : function(){
        this._super();
        this.gamingSceneUI = new GamingSceneUI();
        this.addChild(this.gamingSceneUI,-1);
        this.bake();
    },
    Initialize : function(){
        this.gamingSceneUI.Initialize();
    },
    GameOver : function(score,bestScore,mainScene,wave){
        this.gameOverUI = new GameOverUI();
        this.gameOverUI.Initialize(score,bestScore,mainScene,wave);
        this.addChild(this.gameOverUI,1);
    },
    onEnter : function(){
        this._super();

    }
});

var GamingSceneUI = cc.Node.extend({
    ccsNode : null,
    bestLabel : null,
    scoreLabel : null,
    scoreAddLabel : null,
    honorSprite : null,
    currentScore : 0,
    operationSprite : null,
    ctor : function(){
        this._super();
        var currentScoreSprite = new cc.Sprite("#scores.png");
        currentScoreSprite.attr({x : SCORE_SPRITE_MAIN.x,y : SCORE_SPRITE_MAIN.y});
        var bestScoreSprite = new cc.Sprite("#best score.png");
        bestScoreSprite.attr({x : BEST_SPRITE_MAIN.x,y : BEST_SPRITE_MAIN.y});
        this.addChild(currentScoreSprite);
        this.addChild(bestScoreSprite);
        this.scoreLabel = new cc.LabelBMFont("0",res.gameScoreFnt);
        this.scoreLabel.attr({x : SCORE_LABEL_MAIN.x,y : SCORE_LABEL_MAIN.y});
        this.bestLabel = new cc.LabelBMFont("0",res.gameOverBestScoreFnt);
        this.bestLabel.attr({x : BEST_LABEL_MAIN.x,y : BEST_LABEL_MAIN.y});
        this.scoreAddLabel = new cc.LabelBMFont("",res.gameScoreFnt);
        this.scoreAddLabel.attr({x : SCOREADD_LABEL.x, y : SCOREADD_LABEL.y});
        var loginButton = new cc.MenuItemSprite(
            new cc.Sprite("#login1.png"),
            new cc.Sprite("#login2.png"),
            /** @expose */
            function(){yw.login({
                'login':function (err, data) {
                    if (err) {
                        //登录失败执行这里
                    } else {
                        //登录成功执行这里
                    }
                },
                'register': function (err, data) {
                    if (err) {
                        //注册失败执行这里
                    } else {
                        //注册成功执行这里
                    }
                }
            });
            AudioManager(res.buttonMp3);
            },
            this
        );
        loginButton.attr({x : LOGIN_BUTTON.x,y : LOGIN_BUTTON.y});
        var mainMenuButton = new cc.MenuItemSprite(
            new cc.Sprite("#home1.png"),
            new cc.Sprite("#home2.png"),
            function(){
                AudioManager(res.buttonMp3);
                cc.director.popScene();
            },
            this
        );
        mainMenuButton.attr({x : MAINMENU_MAIN.x,y : MAINMENU_MAIN.y});
        var muteButton = new cc.MenuItemToggle(
            new cc.MenuItemImage("#volume.png","#volume mute.png"),
            new cc.MenuItemImage("#volume mute.png","#volume.png"),
            function(){
                ENABLE_AUDIO = !ENABLE_AUDIO;
                if(ENABLE_AUDIO)cc.audioEngine.playMusic(res.gameMp3,true);
                else cc.audioEngine.stopMusic(res.gameMp3);
            },
            this
        );
        muteButton.attr({x : MUTE_SPRITE.x,y : MUTE_SPRITE.y});
        var shareButton = new cc.MenuItemSprite(
            new cc.Sprite("#share1.png"),
            new cc.Sprite("#share2.png"),
            function(){
                AudioManager(res.buttonMp3);
                /** @expose */
                yw.openSocialShareMenu({
                    "text" : "我在这个游戏里获得了10000分！快来挑战我吧！",
                    "title" : "#严禁智障患者玩此游戏!#我在这个游戏中获得了" + (Math.random() * 10900 + 4500) + "分，快点来挑战我啊！",
                },function(){});
            },
            this
        );
        shareButton.attr({x : SHARE_MAIN.x,y : SHARE_MAIN.y});
        this.Initialize();
        var operationSprite;
        if(ENVIRONMENT)operationSprite = new cc.Sprite("#guideboard pc.png");
        else operationSprite = new cc.Sprite("#guideboard phone.png");
        operationSprite.attr({x : OPERATION_SPRITE.x,y : OPERATION_SPRITE.y});
        operationSprite.runAction(cc.sequence(cc.delayTime(5),cc.spawn(cc.rotateBy(1.0,180),cc.fadeOut(1.0),cc.scaleTo(1.0,1.5))));
        operationSprite.scaleX = operationSprite.scaleY = 2;
        var mu = new cc.Menu(loginButton,mainMenuButton,muteButton,shareButton);
        this.addChild(mu,1);
        mu.x = mu.y = 0;
        this.addChild(operationSprite);
        this.addChild(this.scoreAddLabel);
        this.addChild(this.scoreLabel);
        this.addChild(this.bestLabel);
    },
    Initialize : function(){
        var bestScore = cc.sys.localStorage.getItem("bestScore") ? cc.sys.localStorage.getItem("bestScore") : 0;
        this.bestLabel.setString(bestScore);
        this.scoreAddLabel.setString("");
    }
});

var GameOverUI = cc.Node.extend({
    totScoreLabel : null,
    bestScoreLabel : null,
    ratingLabel : null,
    noticeLabel : null,
    restartButton : null,
    mainScene : null,
    ionicLabel : null,
    ctor : function(){
        this._super();
        var that = this;
        this.scaleX = this.scaleY = 0;
        this.runAction(cc.scaleTo(0.3,1.0));
        cc.eventManager.addListener({
            event : cc.EventListener.KEYBOARD,
            onKeyPressed : function(keyCode,event){
                if(keyCode == 32)
                    that.Restart();
            }
        },this);
        if(!ENVIRONMENT)
        this.attr({x : -50,y : 100});
        this.totScoreLabel = new cc.LabelBMFont("0",res.gameOverScoreFnt);
        this.totScoreLabel.attr({x : SCORE_LABEL_OVER.x,y : SCORE_LABEL_OVER.y});
        this.bestScoreLabel = new cc.LabelBMFont("0",res.gameOverBestScoreFnt);
        this.bestScoreLabel.attr({x : BEST_LABEL_OVER.x,y : BEST_LABEL_OVER.y});
        this.restartButton = new cc.MenuItemSprite(
            new cc.Sprite("#restart.png"),
            new cc.Sprite("#restart.png"),
            this.Restart,
            this
        );
        this.restartButton.attr({x : RESTART_BUTTON.x,y : RESTART_BUTTON.y});
        var homeButton = new cc.MenuItemSprite(
            new cc.Sprite("#home.png"),
            new cc.Sprite("#home.png"),
            function(){
                AudioManager(res.buttonMp3);
                cc.director.popScene();
            },
            this
        );
        homeButton.attr({x : MAIN_MENU_OVER.x,y : MAIN_MENU_OVER.y});
        var shareButton = new cc.MenuItemSprite(
            new cc.Sprite("#share.png"),
            new cc.Sprite("#share.png"),
            function(){
                AudioManager(res.buttonMp3);
                /** @expose */
                yw.openSocialShareMenu({
                    "text" : "我在这个游戏里获得了10000分！快来挑战我吧！",
                    "title" : "#严禁智障患者玩此游戏!#我在这个游戏中获得了" + (Math.random() * 10900 + 4500) + "分，快点来挑战我啊！",
                },function(){});
            },
            this
        );
        shareButton.attr({x : SHARE_OVER.x,y : SHARE_OVER.y});
        this.ionicLabel = new cc.LabelBMFont("",res.Chinese);
        this.ionicLabel.attr({x : IONIC_LABEL.x,y : IONIC_LABEL.y});
        this.addChild(this.ionicLabel);
        this.addChild(this.totScoreLabel);
        this.addChild(this.bestScoreLabel);
        var mu = new cc.Menu(this.restartButton,homeButton,shareButton);
        mu.x = mu.y = 0;
        this.addChild(mu);
        var gameOverSprite = new cc.Sprite(res.OverPic);
        gameOverSprite.attr({
            x : OVER_PIC_POINT.x,
            y : OVER_PIC_POINT.y
        });
        this.addChild(gameOverSprite,-1);
    },
    Initialize : function(score,bestScore,mainScene,wave){
        this.totScoreLabel.setString(score);
        this.bestScoreLabel.setString(bestScore);
        this.mainScene = mainScene;
        if(bestScore == score)
            /** @expose */
        yw.showRank(score);
        /** @expose */
        else yw.showRank({score : score,description : "成绩：" + score});
        this.Ironic(wave);
    },
    Restart : function(){
        var that = this;
        AudioManager(res.buttonMp3);
        var horizonTalDis = 500;
        var jumpHeight = 100;
        if(!ENVIRONMENT){
            horizonTalDis /= 2;
            jumpHeight/= 2;
        }
        this.ionicLabel.setString("");
        this.mainScene.Initialize();
        this.runAction(cc.sequence(
            cc.jumpBy(0.6,cc.p(horizonTalDis,0),jumpHeight,2),
            cc.callFunc(function(){that.removeFromParent(true);})
        ));
    },
    Ironic : function(score){
        var index;
        if(score <= 2){
            index = parseInt(Math.random() * 7);
            this.ionicLabel.setString(IONIC_SENTENCES[index]);
            index = parseInt(Math.random() * 5);
            AudioManager(res.iron[index]);
        }
        else if(score <= 4){
            index = parseInt(Math.random() * 5 + 8);
            this.ionicLabel.setString(IONIC_SENTENCES[index]);
            index = parseInt(Math.random() * 5);
            AudioManager(res.iron[index]);
        }
        else if(score <= 7){
            index = parseInt(Math.random() * 4 + 13);
            this.ionicLabel.setString(IONIC_SENTENCES[index]);
            index = parseInt(Math.random() * 2 + 6);
            AudioManager(res.iron[index]);
        }
        else if(score <= 12){
            index = parseInt(Math.random() * 3 + 17);
            this.ionicLabel.setString(IONIC_SENTENCES[index]);
                AudioManager(res.iron[7]);
        }
        else if(score <= 20){
            index = parseInt(Math.random() * 2 + 21);
            this.ionicLabel.setString(IONIC_SENTENCES[index]);
                AudioManager(res.iron[8]);
        }
        else this.ionicLabel.setString(IONIC_SENTENCES[22]);
    }
});


var MainMenu = cc.Scene.extend({
    startButton : null,
    background_Image : null,
    size : null,
    ctor : function(){
        this._super();
        this.size = cc.director.getWinSize();
        this.background_Image = new cc.Sprite(res.MainPic);
        this.background_Image.attr({
            x : this.size.width / 2,
            y : this.size.height / 2,
            anchorX : 0.5,
            anchorY : 0.5
        });
        this.addChild(this.background_Image,1);
        this.startButton = new cc.MenuItemSprite(
            new cc.Sprite(res.Start_btn),
            new cc.Sprite(res.Start_btn_p),
            this.Start,
            this
        );
        if(!ENVIRONMENT)
        this.background_Image.scaleY = 1.250;
        var mu = new cc.Menu(this.startButton);
        mu.x = this.size.width / 2;
        mu.y = this.size.height / 3;
        this.addChild(mu,2);
    },
    Start : function(){
        AudioManager(res.buttonMp3);
        cc.director.pushScene(new cc.TransitionProgressRadialCCW(0.5,new PlayerTesterScene()));
    },
    onEnterTransitionDidFinish : function(){
        this._super();
        cc.audioEngine.playMusic(res.gameMp3,true);
    }
});
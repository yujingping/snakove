var PlayerLayer = cc.Layer.extend({
    actionStack : new Array(),
    currentNum : 0,
    totNum : 0,
    player : null,
    isFull : true,
    currentIndex : null,
    allowToMove : true,
    ctor : function(){
        this._super();
        this.player = new cc.Sprite();
        var map = new cc.Sprite(res.map_png);
        map.attr({
            x : MAP_POINT.x,
            y : MAP_POINT.y,
            anchorX : 0.5,
            anchorY : 0.5
        });
        if(!ENVIRONMENT){
            map.scaleX = map.scaleY = 1.5;
            this.player.setScale(1.5);
        }
        this.addChild(map,-1);
        this.Initialize();
        this.addChild(this.player);
    },
    Initialize : function(){
        this.allowToMove = true;
        this.currentNum = this.totNum = 0;
        this.currentIndex = cc.p(2,2);
        this.player.initWithFile(res.Player_front);
        this.player.attr({
            x : StabPos[5].x,
            y : StabPos[5].y
        });
    },
    onEnter : function(){
        this._super();
        var that = this;
        var firstPos = null;
        var isTouched = false;
        cc.eventManager.addListener({
            event : cc.EventListener.KEYBOARD,
            onKeyPressed : function(keyCode,event){
                var correspondingNum;
                if(!that.allowToMove)return;
                switch (keyCode){
                    case Keycode.W :
                        correspondingNum = 0;
                        break;
                    case Keycode.S :
                        correspondingNum = 1;
                        break;
                    case Keycode.A :
                        correspondingNum = 2;
                        break;
                    case Keycode.D :
                        correspondingNum = 3;
                        break;
                    case 38 :
                        correspondingNum = 0;
                        break;
                    case 40 :
                        correspondingNum = 1;
                        break;
                    case 37 :
                        correspondingNum = 2;
                        break;
                    case 39 :
                        correspondingNum = 3;
                        break;
                    default : return;
                }
                var target = event.getCurrentTarget();
                target.Move(correspondingNum);
            }
            },this);
            cc.eventManager.addListener({
                event : cc.EventListener.TOUCH_ONE_BY_ONE,
                onTouchBegan : function(touch,event){
                    if(!that.allowToMove)return;
                    that.isTouched = true;
                    that.firstPos = touch.getLocation();
                    return true;
                },
                onTouchMoved : function(touch,event){
                    if(!that.isTouched)return;
                    var finalPos = touch.getLocation();
                    if(Math.sqrt((that.firstPos.x - finalPos.x) * (that.firstPos.x - finalPos.x) + (that.firstPos.y - finalPos.y) * (that.firstPos.y - finalPos.y)) < 15)return;
                    that.isTouched = false;
                    var positionDeltaX = that.firstPos.x - finalPos.x;
                    var positionDeltaY = that.firstPos.y - finalPos.y;
                    if(positionDeltaY > 0 && Math.abs(positionDeltaY) > Math.abs(positionDeltaX))
                    that.Move(1);
                    else if(positionDeltaY < 0 && Math.abs(positionDeltaY) > Math.abs(positionDeltaX))
                    that.Move(0);
                    else if(positionDeltaX > 0 && Math.abs(positionDeltaX) > Math.abs(positionDeltaY))
                    that.Move(2);
                    else if(positionDeltaX < 0 && Math.abs(positionDeltaX) > Math.abs(positionDeltaY))
                    that.Move(3);
                }
            },this);
    },
    Move : function(num){
        this.actionStack.unshift(num);
        this.totNum++;
        if(this.isFull)this.Mover();
    },
    Mover : function(){
        while(true){
            if(this.currentNum == this.totNum){
                this.isFull = true;
                break;
            }
            this.isFull = false;
            this.currentNum++;
            var num = this.actionStack.shift();
            if(this.currentIndex.x <= 1 && num == 2){this.isFull = true;break;}
            else if(this.currentIndex.x >= 4 && num == 3){this.isFull = true;break;}
            else if(this.currentIndex.y <= 1 && num == 0){this.isFull = true;break;}
            else if(this.currentIndex.y >= 4 && num == 1){this.isFull = true;break;}
            var action,xx,yy;
            xx = (Math.floor(num / 2) == 1) ? 1 : 0;
            yy = (Math.floor(num / 2) == 1) ? 0 : 1;
            if(num == 1 || num == 2){xx = -xx;yy = -yy;}
            //if(num == 1 || num == 0)this.player.initWithFile(res.Player_front);
            //else if(num == 2)this.player.initWithFile(res.Player_left);
            //else this.player.initWithFile(res.Player_right);
            this.currentIndex.x += xx;
            this.currentIndex.y += -yy;
            xx *= PLAYER_STEP_HALF;
            yy *= PLAYER_STEP_HALF;
            action = new cc.MoveBy(PLAYER_STEP_TIME,cc.p(xx,yy));
            this.player.runAction(cc.sequence(cc.spawn(new cc.ScaleTo(PLAYER_STEP_TIME,PLAYER_MOVE_SCALE),action),cc.spawn(action,new cc.ScaleTo(PLAYER_STEP_TIME,PLAYER_FINAL_SCALE))));
        }
    },
    GameOver : function(){
        this.allowToMove = false;
        this.player.initWithFile(res.Player_die);
        //ParticleManager(this,cc.p(this.player.x,this.player.y),1,res.dieFX);
    }
});

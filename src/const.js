var Keycode = {
    W : 87,
    A : 65,
    S : 83,
    D : 68
};

var ENABLE_AUDIO = true;
var SCENE_HEIGHT = 600;
var SCENE_WIDTH = 800;
var DISPLAY_POLICY = cc.ResolutionPolicy.SHOW_ALL;
var PLAYER_STEP_HALF = 30;
var PLAYER_STEP_TIME = 0.03;
var PLAYER_MOVE_SCALE = 0.3;
var BULLET_HALF_LENGTH = 35;
var LASER_HALF_LENGTH = 0;
var GROUND_STAB_HALF = 30;

var ObstacleType = {
    Bullet : 1,
    Laser : 2,
    GroundStab : 3,
    Meteor : 4,
    Notice : 5
};

var AnimationCache;
var BulletPos = [
    cc.p(310,600 + BULLET_HALF_LENGTH),
    cc.p(370,600 + BULLET_HALF_LENGTH),
    cc.p(430,600 + BULLET_HALF_LENGTH),
    cc.p(490,600 + BULLET_HALF_LENGTH),
    cc.p(800 + BULLET_HALF_LENGTH,390),
    cc.p(800 + BULLET_HALF_LENGTH,330),
    cc.p(800 + BULLET_HALF_LENGTH,270),
    cc.p(800 + BULLET_HALF_LENGTH,210),
    cc.p(490,- BULLET_HALF_LENGTH),
    cc.p(430,- BULLET_HALF_LENGTH),
    cc.p(370,- BULLET_HALF_LENGTH),
    cc.p(310,- BULLET_HALF_LENGTH),
    cc.p(- BULLET_HALF_LENGTH,210),
    cc.p(- BULLET_HALF_LENGTH,270),
    cc.p(- BULLET_HALF_LENGTH,330),
    cc.p(- BULLET_HALF_LENGTH,390)
];

var LaserPos = [
    cc.p(310,450),
    cc.p(370,450),
    cc.p(430,450),
    cc.p(490,450),
    cc.p(550,390),
    cc.p(550,330),
    cc.p(550,270),
    cc.p(550,210),
    cc.p(490,150),
    cc.p(430,150),
    cc.p(370,150),
    cc.p(310,150),
    cc.p(250,210),
    cc.p(250,270),
    cc.p(250,330),
    cc.p(250,390)
];

var StabPos = [
    cc.p(310,390),
    cc.p(370,390),
    cc.p(430,390),
    cc.p(490,390),
    cc.p(310,330),
    cc.p(370,330),
    cc.p(430,330),
    cc.p(490,330),
    cc.p(310,270),
    cc.p(370,270),
    cc.p(430,270),
    cc.p(490,270),
    cc.p(310,210),
    cc.p(370,210),
    cc.p(430,210),
    cc.p(490,210)
];

var MeteorPos = [
    cc.p(340,360),
    cc.p(400,360),
    cc.p(460,360),
    cc.p(340,300),
    cc.p(400,300),
    cc.p(460,300),
    cc.p(340,240),
    cc.p(400,240),
    cc.p(460,240)
];

var Animations = {

};

var PLAYER_SCALE = 56;
var BULLET_LENGTH = 40;
var BULLET_WIDTH = 15;

/**
 * Created by Think Pad e431 on 2015/12/13.
 */
var waves = [
    //teach
    {
        waveTime : 32,
        array : [
            {type : ObstacleType.Notice,time : 0,position : 1,duration : 3},
            {type : ObstacleType.Bullet,time : 4,position : 4,duration : 2},
            {type : ObstacleType.Bullet,time : 4,position : 12,duration : 2},
            {type : ObstacleType.Notice,time : 5.8,position : 2,duration : 3},
            {type : ObstacleType.Laser,time : 9.8,position : 0,duration : 2},
            {type : ObstacleType.Notice,time : 12,position : 3,duration : 3},
            {type : ObstacleType.GroundStab,time : 16,position : 15,duration : 1.5},
            {type : ObstacleType.Notice,time : 17.5,position : 4,duration : 3},
            {type : ObstacleType.Meteor,time : 21.5,position : 4,duration : 2},
            {type : ObstacleType.Notice,time : 25,position : 5,duration : 3},
            {type : ObstacleType.Notice,time : 28,position : 6,duration : 3}
        ]
    },
    //easy1-6
    {
        waveTime : 7,
        array : [
            {type : ObstacleType.Bullet,time : 0.8,position : 3,duration : 3},
            {type : ObstacleType.Bullet,time : 2.3,position : 6,duration : 3},
            {type : ObstacleType.Bullet,time : 5.3,position : 2,duration : 3},
            {type : ObstacleType.Bullet,time : 5.9,position : 10,duration : 3},
            {type : ObstacleType.Bullet,time : 6,position : 7,duration : 3.5}
        ]
    },
    {
        waveTime : 8,
        array : [
            {type : ObstacleType.Bullet,time : 0.8,position : 1,duration : 3},
            {type : ObstacleType.Bullet,time : 1.8,position : 15,duration : 2},
            {type : ObstacleType.Bullet,time : 2.6,position : 7,duration : 2.2},
            {type : ObstacleType.Bullet,time : 3.5,position : 10,duration : 2},
            {type : ObstacleType.Bullet,time : 5.3,position : 6,duration : 3},
            {type : ObstacleType.Bullet,time : 6.9,position : 2,duration : 2.8}
        ]
    },
    {
        waveTime : 6,
        array : [
            {type : ObstacleType.Bullet,time : 0.9,position : 12,duration : 2},
            {type : ObstacleType.Bullet,time : 1.8,position : 1,duration : 2.3},
            {type : ObstacleType.Bullet,time : 1.8,position : 8,duration : 2.6},
            {type : ObstacleType.Bullet,time : 2.9,position : 5,duration : 2},
            {type : ObstacleType.Bullet,time : 3.5,position : 10,duration : 2.4}
        ]
    },
    {
        waveTime : 7,
        array : [
            {type : ObstacleType.Bullet,time : 0.7,position : 5,duration : 6.5},
            {type : ObstacleType.Bullet,time : 0.8,position : 0,duration : 1.8},
            {type : ObstacleType.Bullet,time : 1,position : 13,duration : 6},
            {type : ObstacleType.Bullet,time : 1.8,position : 1,duration : 1.8},
            {type : ObstacleType.Bullet,time : 2.8,position : 2,duration : 1.8},
            {type : ObstacleType.Bullet,time : 3.8,position : 7,duration : 1.9},
            {type : ObstacleType.Bullet,time : 4.8,position : 6,duration : 1.8},
            {type : ObstacleType.Bullet,time : 5.8,position : 5,duration : 1.7}
        ]
    },
    //2
    {
        waveTime : 8,
        array : [
            {type : ObstacleType.Bullet,time : 0.8,position : 0,duration : 6},
            {type : ObstacleType.Bullet,time : 1.4,position : 1,duration : 6},
            {type : ObstacleType.Bullet,time : 2,position : 3,duration : 6},
            {type : ObstacleType.Bullet,time : 2.5,position : 14,duration : 6},
            {type : ObstacleType.Bullet,time : 2.5,position : 13,duration : 6},
            {type : ObstacleType.Bullet,time : 2.6,position : 6,duration : 1.9},
            {type : ObstacleType.Bullet,time : 3.5,position : 12,duration : 6},
            {type : ObstacleType.Bullet,time : 4.2,position : 9,duration : 3},
            {type : ObstacleType.Bullet,time : 5.3,position : 4,duration : 2}
        ]
    },
    {
        waveTime : 8.6,
        array : [
            {type : ObstacleType.Bullet,time : 0.8,position : 0,duration : 5},
            {type : ObstacleType.Bullet,time : 1.4,position : 7,duration : 2.4},
            {type : ObstacleType.Bullet,time : 1.8,position : 0,duration : 5},
            {type : ObstacleType.Bullet,time : 2.2,position : 4,duration : 3},
            {type : ObstacleType.Bullet,time : 2.8,position : 0,duration : 5},
            {type : ObstacleType.Bullet,time : 3.5,position : 6,duration : 5},
            {type : ObstacleType.Bullet,time : 3.8,position : 14,duration : 5},
            {type : ObstacleType.Bullet,time : 4.8,position : 6,duration : 2.5},
            {type : ObstacleType.Bullet,time : 4.8,position : 14,duration : 5},
            {type : ObstacleType.Bullet,time : 5,position : 9,duration : 4.3},
        ]
    },
    //normal7-15
    {
        waveTime : 4,
        array : [
            {type : ObstacleType.GroundStab,time : 0.2,position : 0,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 0.4,position : 10,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 0.6,position : 11,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 0.8,position : 5,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1,position : 3,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.2,position : 12,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.4,position : 8,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.6,position : 7,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.8,position : 9,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2,position : 2,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2.2,position : 14,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2.4,position : 0,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2.6,position : 6,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2.8,position : 10,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 3,position : 3,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 3.3,position : 12,duration : 1.5}
        ]
    },
    {
        waveTime : 9.5,
        array : [
            {type : ObstacleType.Bullet,time : 0.5,position : 13,duration : 1.9},
            {type : ObstacleType.Bullet,time : 0.8,position : 2,duration : 2},
            {type : ObstacleType.Bullet,time : 1.5,position : 14,duration : 1.7},
            {type : ObstacleType.Bullet,time : 2.4,position : 6,duration : 2},
            {type : ObstacleType.Bullet,time : 3.3,position : 8,duration : 2},
            {type : ObstacleType.Bullet,time : 4.1,position : 12,duration : 2},
            {type : ObstacleType.Bullet,time : 4.3,position : 0,duration : 1.8},
            {type : ObstacleType.Bullet,time : 5.9,position : 3,duration : 2},
            {type : ObstacleType.Bullet,time : 5.9,position : 7,duration : 2},
            {type : ObstacleType.Bullet,time : 6.3,position : 11,duration : 2},
            {type : ObstacleType.Bullet,time : 7,position : 15,duration : 2},
            {type : ObstacleType.Bullet,time : 7.7,position : 10,duration : 2}
        ]
    },
    //3
    {
        waveTime : 7.5,
        array : [
            {type : ObstacleType.Bullet,time : 0.5,position : 13,duration : 1.9},
            {type : ObstacleType.GroundStab,time : 0.8,position : 5,duration : 3},
            {type : ObstacleType.Bullet,time : 1.9,position : 8,duration : 2},
            {type : ObstacleType.Bullet,time : 2.4,position : 12,duration : 3},
            {type : ObstacleType.Bullet,time : 3,position : 6,duration : 2.5},
            {type : ObstacleType.GroundStab,time : 4,position : 14,duration : 3},
            {type : ObstacleType.Bullet,time : 4.2,position : 6,duration : 2.6},
            {type : ObstacleType.Bullet,time : 5.3,position : 6,duration : 2}
        ]
    },
    {
        waveTime : 7,
        array : [
            {type : ObstacleType.GroundStab,time : 0.8,position : 5,duration : 3},
            {type : ObstacleType.GroundStab,time : 1.8,position : 6,duration : 3},
            {type : ObstacleType.GroundStab,time : 3,position : 14,duration : 3},
            {type : ObstacleType.GroundStab,time : 3,position : 2,duration : 3},
            {type : ObstacleType.GroundStab,time : 4,position : 8,duration : 3},
            {type : ObstacleType.GroundStab,time : 4,position : 4,duration : 3},
            {type : ObstacleType.GroundStab,time : 4,position : 7,duration : 3},
            {type : ObstacleType.GroundStab,time : 5,position : 0,duration : 3},
            {type : ObstacleType.GroundStab,time : 5,position : 2,duration : 3},
            {type : ObstacleType.GroundStab,time : 5,position : 9,duration : 3},
            {type : ObstacleType.GroundStab,time : 5,position : 13,duration : 3},
            {type : ObstacleType.GroundStab,time : 5,position : 11,duration : 3}
        ]
    },
    {
        waveTime : 9,
        array : [
            {type : ObstacleType.Bullet,time : 0.5,position : 0,duration : 3},
            {type : ObstacleType.Bullet,time : 0.5,position : 12,duration : 3},
            {type : ObstacleType.Bullet,time : 0.5,position : 8,duration : 3},
            {type : ObstacleType.Bullet,time : 0.5,position : 4,duration : 3},
            {type : ObstacleType.GroundStab,time : 3.1,position : 0,duration : 3},
            {type : ObstacleType.GroundStab,time : 3.1,position : 3,duration : 3},
            {type : ObstacleType.GroundStab,time : 3.1,position : 12,duration : 3},
            {type : ObstacleType.GroundStab,time : 3.1,position : 15,duration : 3},
            {type : ObstacleType.Bullet,time : 5,position : 1,duration : 3},
            {type : ObstacleType.Bullet,time : 5,position : 13,duration : 3},
            {type : ObstacleType.Bullet,time : 5,position : 9,duration : 3},
            {type : ObstacleType.Bullet,time : 5,position : 5,duration : 3},
            {type : ObstacleType.GroundStab,time : 7.3,position : 1,duration : 3},
            {type : ObstacleType.GroundStab,time : 7.3,position : 2,duration : 3},
            {type : ObstacleType.GroundStab,time : 7.3,position : 4,duration : 3},
            {type : ObstacleType.GroundStab,time : 7.3,position : 8,duration : 3},
            {type : ObstacleType.GroundStab,time : 7.3,position : 7,duration : 3},
            {type : ObstacleType.GroundStab,time : 7.3,position : 11,duration : 3},
            {type : ObstacleType.GroundStab,time : 7.3,position : 13,duration : 3},
            {type : ObstacleType.GroundStab,time : 7.3,position : 14,duration : 3}
        ]
    },
    //4
    {
        waveTime : 8,
        array : [
            {type : ObstacleType.Laser,time : 0.5,position : 0,duration : 2},
            {type : ObstacleType.Laser,time : 1,position : 1,duration : 2},
            {type : ObstacleType.Laser,time : 1.5,position : 2,duration : 2},
            {type : ObstacleType.Laser,time : 2,position : 4,duration : 2},
            {type : ObstacleType.Laser,time : 2.5,position : 5,duration : 2},
            {type : ObstacleType.Laser,time : 3,position : 6,duration : 2},
            {type : ObstacleType.Laser,time : 3.5,position : 8,duration : 2},
            {type : ObstacleType.Laser,time : 4,position : 9,duration : 2},
            {type : ObstacleType.Laser,time : 4.5,position : 10,duration : 2}
        ]
    },
    {
        waveTime : 8.5,
        array : [
            {type : ObstacleType.Laser,time : 0.8,position : 1,duration : 4},
            {type : ObstacleType.Bullet,time : 1.2,position :14,duration : 3},
            {type : ObstacleType.GroundStab,time : 1.8,position : 6,duration : 4},
            {type : ObstacleType.Bullet,time : 1.8,position : 7,duration : 2.8},
            {type : ObstacleType.Bullet,time : 2.7,position : 3,duration : 2.8},
            {type : ObstacleType.Bullet,time : 2.7,position : 15,duration : 2.8},
            {type : ObstacleType.Bullet,time : 3.8,position : 9,duration : 3},
            {type : ObstacleType.Laser,time : 4.4,position : 15,duration : 1.8},
            {type : ObstacleType.Laser,time : 4.4,position : 12,duration : 1.8},
            {type : ObstacleType.GroundStab,time : 4.4,position : 4,duration : 4},
            {type : ObstacleType.GroundStab,time : 4.4,position : 8,duration : 4},
            {type : ObstacleType.GroundStab,time : 5.3,position : 5,duration : 4},
            {type : ObstacleType.GroundStab,time : 5.3,position : 9,duration : 4},
            {type : ObstacleType.GroundStab,time : 6.2,position : 6,duration : 4},
            {type : ObstacleType.GroundStab,time : 6.2,position : 10,duration : 4},
            {type : ObstacleType.GroundStab,time : 7.2,position : 7,duration : 4},
            {type : ObstacleType.GroundStab,time : 7.2,position : 11,duration : 4}
        ]
    },
    {
        waveTime : 7.5,
        array : [
            {type : ObstacleType.Laser,time : 0.5,position : 5,duration : 3},
            {type : ObstacleType.Bullet,time : 1.5,position : 0,duration : 3},
            {type : ObstacleType.Bullet,time : 1.5,position : 5,duration : 3},
            {type : ObstacleType.Bullet,time : 2.5,position : 13,duration : 2.7},
            {type : ObstacleType.Bullet,time : 2.7,position : 10,duration : 3},
            {type : ObstacleType.Bullet,time : 3.5,position : 14,duration : 3.5},
            {type : ObstacleType.Bullet,time : 3.8,position : 13,duration : 3},
            {type : ObstacleType.Laser,time : 4,position : 5,duration : 3},
            {type : ObstacleType.Bullet,time : 4.2,position : 2,duration : 3.1},
            {type : ObstacleType.Bullet,time : 4.4,position : 7,duration : 2.6},
            {type : ObstacleType.Bullet,time : 4.9,position : 12,duration : 3.2},
            {type : ObstacleType.Bullet,time : 4.9,position : 11,duration : 3.7},
            {type : ObstacleType.Bullet,time : 5.1,position : 8,duration : 2.5}
        ]
    },
    {
        waveTime : 8.2,
        array : [
            {type : ObstacleType.Laser,time : 0.5,position : 13,duration : 3},
            {type : ObstacleType.Bullet,time : 1.5,position : 4,duration : 3},
            {type : ObstacleType.Bullet,time : 1.5,position : 9,duration : 3},
            {type : ObstacleType.Bullet,time : 2.5,position : 1,duration : 2.7},
            {type : ObstacleType.Bullet,time : 2.7,position : 14,duration : 3},
            {type : ObstacleType.Bullet,time : 3.5,position : 2,duration : 3.5},
            {type : ObstacleType.Bullet,time : 3.8,position : 1,duration : 3},
            {type : ObstacleType.Laser,time : 3.8,position : 13,duration : 3},
            {type : ObstacleType.Bullet,time : 4.2,position : 6,duration : 3.1},
            {type : ObstacleType.Bullet,time : 4.4,position : 11,duration : 2.6},
            {type : ObstacleType.Bullet,time : 4.9,position : 0,duration : 3.2},
            {type : ObstacleType.Bullet,time : 4.9,position : 1,duration : 3.7},
            {type : ObstacleType.Bullet,time : 5.1,position : 12,duration : 2.5},
            {type : ObstacleType.Bullet,time : 5.1,position : 0,duration : 3}
        ]
    },
    //hard
    //5
    {
        waveTime : 2.5,
        array : [
            {type : ObstacleType.GroundStab,time : 0.5,position : 0,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 0.5,position : 3,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 0.5,position : 12,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 0.5,position : 15,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.2,position : 5,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.2,position : 6,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.2,position : 9,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.2,position : 10,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2,position : 1,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2,position : 2,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2,position : 7,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2,position : 11,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2,position : 13,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2,position : 14,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2,position : 4,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2,position : 8,duration : 1.5}
        ]
    },
    {
        waveTime : 6.3,
        array : [
            {type : ObstacleType.Bullet,time : 0.8,position : 5,duration : 3},
            {type : ObstacleType.GroundStab,time : 1.7,position : 3,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.8,position : 8,duration : 1.5},
            {type : ObstacleType.Bullet,time : 1.8,position : 4,duration : 3},
            {type : ObstacleType.Laser,time : 2.2,position : 0,duration : 2},
            {type : ObstacleType.Meteor,time : 2.3,position : 3,duration : 2},
            {type : ObstacleType.Bullet,time : 2.6,position : 7,duration : 4},
            {type : ObstacleType.Meteor,time : 2.9,position : 2,duration : 2},
            {type : ObstacleType.Meteor,time : 3.8,position : 6,duration : 2},
            {type : ObstacleType.Bullet,time : 3.8,position : 10,duration : 2.4},
            {type : ObstacleType.Laser,time : 4,position : 5,duration : 2}
        ]
    },
    {
        waveTime : 4.5,
        array : [
            {type : ObstacleType.GroundStab,time : 0.2,position : 1,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 0.6,position : 5,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1,position : 9,duration : 1.5},
            {type : ObstacleType.Meteor,time : 1.2,position : 0,duration : 2},
            {type : ObstacleType.GroundStab,time : 1.4,position : 10,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.8,position : 8,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2.2,position : 9,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2.6,position : 10,duration : 1.5},
            {type : ObstacleType.Meteor,time : 2.7,position : 6,duration : 2},
            {type : ObstacleType.GroundStab,time : 3,position : 11,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 3.4,position : 3,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 3.8,position : 6,duration : 1.5},
            {type : ObstacleType.Meteor,time : 3.9,position : 2,duration : 2},
            {type : ObstacleType.GroundStab,time : 4.2,position : 9,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 4.6,position : 12,duration : 1.5}
        ]
    },
    {
        waveTime : 5.5,
        array : [
            {type : ObstacleType.Bullet,time : 0.1,position : 3,duration : 3},
            {type : ObstacleType.Bullet,time : 0.1,position : 6,duration : 2.7},
            {type : ObstacleType.Meteor,time : 1,position : 4,duration : 2},
            {type : ObstacleType.Bullet,time : 1.2,position : 0,duration : 3.2},
            {type : ObstacleType.GroundStab,time : 1.7,position : 2,duration : 1.5},
            {type : ObstacleType.Bullet,time : 2,position : 11,duration : 2.4},
            {type : ObstacleType.Meteor,time : 2.5,position : 0,duration : 2},
            {type : ObstacleType.GroundStab,time : 2.6,position : 3,duration : 1.5},
            {type : ObstacleType.Meteor,time : 3.4,position : 7,duration : 2},
            {type : ObstacleType.GroundStab,time : 3.5,position : 1,duration : 1.5},
            {type : ObstacleType.Meteor,time : 4.5,position : 5,duration : 2}
        ]
    },
    {
        waveTime : 6.3,
        array : [
            {type : ObstacleType.Meteor,time : 1,position : 0,duration : 2},
            {type : ObstacleType.Meteor,time : 2,position : 5,duration : 2},
            {type : ObstacleType.Meteor,time : 2,position : 6,duration : 2},
            {type : ObstacleType.Meteor,time : 2.6,position : 8,duration : 2},
            {type : ObstacleType.Meteor,time : 3.2,position : 2,duration : 2},
            {type : ObstacleType.Meteor,time : 4.1,position : 4,duration : 2},
            {type : ObstacleType.Meteor,time : 4.7,position : 3,duration : 2},
            {type : ObstacleType.Meteor,time : 5.2,position : 7,duration : 2}
        ]
    },
    {
        waveTime : 5.4,
        array : [
            {type : ObstacleType.Meteor,time : 1,position : 0,duration : 2},
            {type : ObstacleType.GroundStab,time : 1.7,position : 10,duration : 1.5},
            {type : ObstacleType.Laser,time : 2.2,position : 15,duration : 2.3},
            {type : ObstacleType.Laser,time : 2.4,position : 12,duration : 2},
            {type : ObstacleType.Laser,time : 2.6,position : 1,duration : 2.1},
            {type : ObstacleType.Meteor,time : 2.7,position : 7,duration : 2},
            {type : ObstacleType.GroundStab,time : 3.3,position : 2,duration : 1.5},
            {type : ObstacleType.Bullet,time : 3.5,position : 4,duration : 2.2},
            {type : ObstacleType.Bullet,time : 3.5,position : 1,duration : 3},
            {type : ObstacleType.Bullet,time : 3.7,position : 2,duration : 2.4},
            {type : ObstacleType.Bullet,time : 3.8,position : 12,duration : 1.8}
        ]
    },
    {
        waveTime : 5.4,
        array : [
            {type : ObstacleType.GroundStab,time : 0.1,position : 4,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 0.3,position : 11,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 0.4,position : 13,duration : 1.5},
            {type : ObstacleType.Meteor,time : 1,position : 3,duration : 2},
            {type : ObstacleType.Bullet,time : 1.4,position : 10,duration : 2.8},
            {type : ObstacleType.Bullet,time : 1.4,position : 7,duration : 2.2},
            {type : ObstacleType.GroundStab,time : 1.6,position : 9,duration : 1.5},
            {type : ObstacleType.Bullet,time : 1.9,position : 4,duration : 2.6},
            {type : ObstacleType.GroundStab,time : 2,position : 3,duration : 1.5},
            {type : ObstacleType.Bullet,time : 2.5,position : 0,duration : 3},
            {type : ObstacleType.Bullet,time : 3,position : 0,duration : 2.2},
            {type : ObstacleType.Meteor,time : 3.6,position : 4,duration : 2}
        ]
    },
    {
        waveTime : 5,
        array : [
            {type : ObstacleType.GroundStab,time : 0.1,position : 4,duration : 1.5},
            {type : ObstacleType.Laser,time : 1.1,position : 3,duration : 2},
            {type : ObstacleType.GroundStab,time : 1.3,position : 11,duration : 1.5},
            {type : ObstacleType.Bullet,time : 1.7,position : 14,duration : 3},
            {type : ObstacleType.Meteor,time : 2.1,position : 4,duration : 2},
            {type : ObstacleType.Laser,time : 2.3,position : 7,duration : 2},
            {type : ObstacleType.Bullet,time : 2.7,position : 3,duration : 2.8},
            {type : ObstacleType.GroundStab,time : 2.7,position : 0,duration : 1.5},
            {type : ObstacleType.Bullet,time : 3,position : 15,duration : 2.3}
        ]
    },
    {
        waveTime : 4.7,
        array : [
            {type : ObstacleType.Laser,time : 0.2,position : 0,duration : 1.8},
            {type : ObstacleType.Bullet,time : 0.2,position : 12,duration : 1.9},
            {type : ObstacleType.Laser,time : 0.2,position : 3,duration : 1.8},
            {type : ObstacleType.GroundStab,time : 1.9,position : 10,duration : 1.5},
            {type : ObstacleType.Laser,time : 2.4,position : 15,duration : 1.8},
            {type : ObstacleType.Meteor,time : 2.3,position : 6,duration : 2},
            {type : ObstacleType.Laser,time : 2.4,position : 7,duration : 1.8},
            {type : ObstacleType.Meteor,time : 3,position : 2,duration : 2},
            {type : ObstacleType.Bullet,time : 3.1,position : 15,duration : 1.9},
            {type : ObstacleType.Bullet,time : 3.2,position : 9,duration : 1.8},
            {type : ObstacleType.Bullet,time : 3.5,position : 3,duration : 1.2}
        ]
    },
    {
        waveTime : 5.3,
        array : [
            {type : ObstacleType.GroundStab,time : 0.1,position : 0,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 0.2,position : 6,duration : 1.5},
            {type : ObstacleType.Meteor,time : 0.5,position : 3,duration : 2},
            {type : ObstacleType.GroundStab,time : 0.8,position : 15,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.2,position : 3,duration : 1.5},
            {type : ObstacleType.Meteor,time : 1.5,position : 8,duration : 2},
            {type : ObstacleType.GroundStab,time : 1.9,position : 9,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.9,position : 12,duration : 1.5},
            {type : ObstacleType.Meteor,time : 2.7,position : 0,duration : 2},
            {type : ObstacleType.Meteor,time : 2.9,position : 4,duration : 2},
            {type : ObstacleType.GroundStab,time :3.1,position : 7,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 3.2,position : 13,duration : 1.5},
            {type : ObstacleType.Meteor,time : 3.6,position : 2,duration : 2},
            {type : ObstacleType.GroundStab,time :3.9,position : 8,duration : 1.5}
        ]
    },
    {
        waveTime : 2.4,
        array : [
            {type : ObstacleType.GroundStab,time : 0.1,position : 5,duration : 1.5},
            {type : ObstacleType.Bullet,time : 0.7,position : 1,duration : 2.8},
            {type : ObstacleType.Bullet,time : 1.0,position : 4,duration : 4},
            {type : ObstacleType.Meteor,time : 1.2,position : 6,duration : 2},
            {type : ObstacleType.Laser,time : 1.5,position : 1,duration : 1},
            {type : ObstacleType.Laser,time : 1.8,position : 13,duration : 1.7},
            {type : ObstacleType.GroundStab,time : 2,position : 15,duration : 1.5}
        ]
    },
    {
        waveTime : 3.3,
        array : [
            {type : ObstacleType.Meteor,time : 0.1,position : 4,duration : 2},
            {type : ObstacleType.GroundStab,time : 0.2,position : 11,duration : 1.5},
            {type : ObstacleType.Bullet,time : 0.5,position : 2,duration : 3.3},
            {type : ObstacleType.Bullet,time : 0.6,position : 13,duration : 2.1},
            {type : ObstacleType.Bullet,time : 0.8,position : 11,duration : 4},
            {type : ObstacleType.Meteor,time : 0.7,position : 6,duration : 2},
            {type : ObstacleType.Bullet,time : 1.6,position : 0,duration : 3.1},
            {type : ObstacleType.Bullet,time : 1.8,position : 3,duration : 2.3},
            {type : ObstacleType.Bullet,time : 2,position : 7,duration : 2.5},
            {type : ObstacleType.Bullet,time : 2.1,position : 15,duration : 3.4},
            {type : ObstacleType.Meteor,time : 2.3,position : 1,duration : 2},
            {type : ObstacleType.Bullet,time : 2.4,position : 12,duration : 2}
        ]
    },
    {
        waveTime : 2.5,
        array : [
            {type : ObstacleType.GroundStab,time : 0.1,position : 10,duration : 1.5},
            {type : ObstacleType.Bullet,time : 0.7,position : 9,duration : 2.8},
            {type : ObstacleType.Bullet,time : 1.0,position : 12,duration : 4},
            {type : ObstacleType.Meteor,time : 1.2,position : 2,duration : 2},
            {type : ObstacleType.Laser,time : 1.5,position : 9,duration : 1},
            {type : ObstacleType.Laser,time : 1.8,position : 5,duration : 1.7},
            {type : ObstacleType.GroundStab,time : 2,position : 0,duration : 1.5}
        ]
    },
    {
        waveTime : 4,
        array : [
            {type : ObstacleType.Laser,time : 0.2,position : 8,duration : 1.8},
            {type : ObstacleType.Bullet,time : 0.2,position : 4,duration : 1.9},
            {type : ObstacleType.Laser,time : 0.2,position : 11,duration : 1.8},
            {type : ObstacleType.GroundStab,time : 1.9,position : 5,duration : 1.5},
            {type : ObstacleType.Laser,time : 2.4,position : 7,duration : 1.8},
            {type : ObstacleType.Meteor,time : 2.3,position : 2,duration : 2},
            {type : ObstacleType.Laser,time : 2.4,position : 15,duration : 1.8},
            {type : ObstacleType.Meteor,time : 3,position : 6,duration : 2},
            {type : ObstacleType.Bullet,time : 3.1,position : 7,duration : 1.9},
            {type : ObstacleType.Bullet,time : 3.2,position : 1,duration : 1.8},
            {type : ObstacleType.Bullet,time : 3.5,position : 11,duration : 1.2}
        ]
    },
    {
        waveTime : 4.2,
        array : [
            {type : ObstacleType.Laser,time : 0.6,position : 1,duration : 1},
            {type : ObstacleType.Meteor,time : 1.2,position :0,duration : 2},
            {type : ObstacleType.Laser,time : 1.5,position : 13,duration : 1},
            {type : ObstacleType.Meteor,time : 1.8,position : 7,duration : 2},
            {type : ObstacleType.Meteor,time : 2.2,position : 6,duration : 2},
            {type : ObstacleType.Laser,time : 2.5,position : 7,duration : 1},
            {type : ObstacleType.Meteor,time : 2.8,position : 2,duration : 2},
            {type : ObstacleType.Meteor,time : 3.2,position : 4,duration : 2},
            {type : ObstacleType.Laser,time : 3.4,position : 3,duration : 1},
            {type : ObstacleType.Laser,time : 4,position : 14,duration : 1}
        ]
    },
    {
        waveTime : 2.3,
        array : [
            {type : ObstacleType.Bullet,time : 0.2,position : 2,duration : 1.9},
            {type : ObstacleType.Bullet,time : 0.3,position : 12,duration : 1.9},
            {type : ObstacleType.Bullet,time : 0.8,position : 14,duration : 1.8},
            {type : ObstacleType.Bullet,time : 1,position : 10,duration : 1.5},
            {type : ObstacleType.Bullet,time :1.2,position : 4,duration : 1.8},
            {type : ObstacleType.Bullet,time :1.3,position : 0,duration : 6},
            {type : ObstacleType.Bullet,time :1.4,position : 7,duration : 2},
            {type : ObstacleType.Bullet,time : 1.5,position : 12,duration : 1.9},
            {type : ObstacleType.Bullet,time : 1.9,position : 1,duration : 1.4}
        ]
    },
    {
        waveTime : 2.4,
        array : [
            {type : ObstacleType.Bullet,time : 0.2,position : 10,duration : 1.9},
            {type : ObstacleType.Bullet,time : 0.3,position : 4,duration : 1.9},
            {type : ObstacleType.Bullet,time : 0.8,position : 6,duration : 1.8},
            {type : ObstacleType.Bullet,time : 1,position : 2,duration : 1.5},
            {type : ObstacleType.Bullet,time :1.2,position : 12,duration : 1.8},
            {type : ObstacleType.Bullet,time :1.3,position : 8,duration : 6},
            {type : ObstacleType.Bullet,time :1.4,position : 15,duration : 2},
            {type : ObstacleType.Bullet,time : 1.5,position : 4,duration : 1.9},
            {type : ObstacleType.Bullet,time : 1.9,position : 9,duration : 1.4}
        ]
    },
    {
        waveTime : 4,
        array : [
            {type : ObstacleType.Bullet,time : 0.2,position : 10,duration : 6},
            {type : ObstacleType.Bullet,time : 0.3,position : 4,duration : 6},
            {type : ObstacleType.GroundStab,time : 0.5,position : 15,duration : 1.5},
            {type : ObstacleType.Bullet,time : 0.8,position : 6,duration : 6},
            {type : ObstacleType.Meteor,time : 0.9,position : 0,duration : 2},
            {type : ObstacleType.Bullet,time : 1,position : 2,duration : 6},
            {type : ObstacleType.GroundStab,time : 1.1,position : 8,duration : 1.5},
            {type : ObstacleType.Bullet,time :1.3,position : 8,duration : 6},
            {type : ObstacleType.Meteor,time : 1.4,position : 4,duration : 2},
            {type : ObstacleType.Bullet,time :1.4,position : 15,duration : 6},
            {type : ObstacleType.GroundStab,time : 1.5,position : 3,duration : 1.5},
            {type : ObstacleType.Bullet,time : 1.5,position : 4,duration : 6},
            {type : ObstacleType.GroundStab,time : 1.8,position : 9,duration : 1.5},
            {type : ObstacleType.Meteor,time : 2,position : 2,duration : 2},
            {type : ObstacleType.GroundStab,time : 2.6,position : 14,duration : 1.5},
            {type : ObstacleType.Meteor,time : 3.5,position : 6,duration : 2}
        ]
    },
    {
        waveTime : 4,
        array : [
            {type : ObstacleType.Bullet,time : 0.2,position : 2,duration : 6},
            {type : ObstacleType.Bullet,time : 0.3,position : 12,duration : 6},
            {type : ObstacleType.GroundStab,time : 0.5,position : 0,duration : 1.5},
            {type : ObstacleType.Bullet,time : 0.8,position : 14,duration : 6},
            {type : ObstacleType.Meteor,time : 0.9,position : 8,duration : 2},
            {type : ObstacleType.Bullet,time : 1,position : 10,duration : 6},
            {type : ObstacleType.GroundStab,time : 1.1,position : 7,duration : 1.5},
            {type : ObstacleType.Bullet,time :1.3,position : 0,duration : 6},
            {type : ObstacleType.Meteor,time : 1.4,position : 4,duration : 2},
            {type : ObstacleType.Bullet,time :1.4,position : 7,duration : 6},
            {type : ObstacleType.GroundStab,time : 1.5,position : 12,duration : 1.5},
            {type : ObstacleType.Bullet,time : 1.5,position : 12,duration : 6},
            {type : ObstacleType.GroundStab,time : 1.8,position : 6,duration : 1.5},
            {type : ObstacleType.Meteor,time : 2,position : 6,duration : 2},
            {type : ObstacleType.GroundStab,time : 2.6,position : 1,duration : 1.5},
            {type : ObstacleType.Meteor,time : 3.5,position : 2,duration : 2}
        ]
    },
    {
        waveTime : 3.8,
        array : [
            {type : ObstacleType.Laser,time : 0.4,position : 0,duration : 0.8},
            {type : ObstacleType.Laser,time : 0.8,position : 14,duration : 0.8},
            {type : ObstacleType.Laser,time : 1.2,position : 8,duration : 0.8},
            {type : ObstacleType.Laser,time : 1.4,position : 6,duration : 0.8},
            {type : ObstacleType.Laser,time : 1.8,position : 4,duration : 0.8},
            {type : ObstacleType.Laser,time : 2.2,position : 2,duration : 0.8},
            {type : ObstacleType.Laser,time : 2.7,position : 11,duration : 0.8},
            {type : ObstacleType.Laser,time : 3,position : 3,duration : 0.8},
            {type : ObstacleType.Laser,time : 3.4,position : 15,duration : 0.7}
        ]
    },
    //35
    {
        waveTime : 2.9,
        array : [
            {type : ObstacleType.Meteor,time : 0.5,position : 4,duration : 2},
            {type : ObstacleType.Bullet,time : 0.6,position : 0,duration : 1.9},
            {type : ObstacleType.GroundStab,time : 1.2,position : 15,duration : 1.5},
            {type : ObstacleType.Bullet,time : 1.3,position : 2,duration : 2.6},
            {type : ObstacleType.GroundStab,time : 1.7,position : 3,duration : 1.5},
            {type : ObstacleType.Meteor,time : 1.8,position : 6,duration : 2},
            {type : ObstacleType.Bullet,time : 1.9,position : 5,duration : 3.1},
            {type : ObstacleType.Meteor,time : 2,position : 5,duration : 2},
            {type : ObstacleType.GroundStab,time : 2.3,position : 8,duration : 1.5},
            {type : ObstacleType.Meteor,time : 2.4,position : 1,duration : 2}
        ]
    },
    //36
    {
        waveTime : 3.2,
        array : [
            {type : ObstacleType.Meteor,time : 0.5,position : 4,duration : 2},
            {type : ObstacleType.Bullet,time : 0.6,position : 8,duration : 1.9},
            {type : ObstacleType.GroundStab,time : 1.2,position : 0,duration : 1.5},
            {type : ObstacleType.Bullet,time : 1.3,position : 10,duration : 2.6},
            {type : ObstacleType.GroundStab,time : 1.7,position : 12,duration : 1.5},
            {type : ObstacleType.Meteor,time : 1.8,position : 2,duration : 2},
            {type : ObstacleType.Bullet,time : 1.9,position : 13,duration : 3.1},
            {type : ObstacleType.Meteor,time : 2,position : 3,duration : 2},
            {type : ObstacleType.GroundStab,time : 2.3,position : 7,duration : 1.5},
            {type : ObstacleType.Meteor,time : 2.4,position : 7,duration : 2}
        ]
    },
    {
        waveTime : 2.5,
        array : [
            {type : ObstacleType.Bullet,time : 0.2,position : 0,duration : 1.9},
            {type : ObstacleType.Bullet,time : 0.3,position : 4,duration : 1.9},
            {type : ObstacleType.Bullet,time : 0.8,position : 6,duration : 2},
            {type : ObstacleType.Bullet,time : 1,position : 2,duration : 1.5},
            {type : ObstacleType.Bullet,time :1.2,position : 9,duration : 1.8},
            {type : ObstacleType.Bullet,time :1.3,position : 15,duration : 6},
            {type : ObstacleType.Bullet,time :1.4,position : 1,duration : 2},
            {type : ObstacleType.Bullet,time : 1.5,position : 4,duration : 1.9},
            {type : ObstacleType.Bullet,time : 1.9,position : 7,duration : 1.7}
        ]
    },
    {
        waveTime : 2.6,
        array : [
            {type : ObstacleType.Bullet,time : 0.2,position : 8,duration : 1.9},
            {type : ObstacleType.Bullet,time : 0.3,position : 12,duration : 1.9},
            {type : ObstacleType.Bullet,time : 0.8,position : 14,duration : 2},
            {type : ObstacleType.Bullet,time : 1,position : 10,duration : 1.5},
            {type : ObstacleType.Bullet,time :1.2,position : 1,duration : 1.8},
            {type : ObstacleType.Bullet,time :1.3,position : 7,duration : 6},
            {type : ObstacleType.Bullet,time :1.4,position : 9,duration : 2},
            {type : ObstacleType.Bullet,time : 1.5,position : 12,duration : 1.9},
            {type : ObstacleType.Bullet,time : 1.9,position : 15,duration : 1.7}
        ]
    },
    {
        waveTime : 3.4,
        array : [
            {type : ObstacleType.Meteor,time : 0.5,position : 0,duration : 2},
            {type : ObstacleType.GroundStab,time : 0.8,position : 7,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.2,position : 13,duration : 1.5},
            {type : ObstacleType.Meteor,time : 1.5,position : 8,duration : 2},
            {type : ObstacleType.GroundStab,time : 1.7,position : 2,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.7,position : 9,duration : 1.5},
            {type : ObstacleType.Meteor,time : 1.9,position : 6,duration : 2},
            {type : ObstacleType.GroundStab,time : 2.3,position : 6,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2.3,position : 11,duration : 1.5},
            {type : ObstacleType.Meteor,time : 2.6,position : 5,duration : 2},
            {type : ObstacleType.GroundStab,time : 2.7,position : 14,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2.7,position : 1,duration : 1.5},
            {type : ObstacleType.Bullet,time : 3,position : 4,duration : 1.4},
            {type : ObstacleType.Bullet,time : 3,position : 7,duration : 1.4}
        ]
    },
    //40
    {
        waveTime : 3.5,
        array : [
            {type : ObstacleType.Meteor,time : 0.5,position : 8,duration : 2},
            {type : ObstacleType.GroundStab,time : 0.8,position :8,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.2,position : 2,duration : 1.5},
            {type : ObstacleType.Meteor,time : 1.5,position : 0,duration : 2},
            {type : ObstacleType.GroundStab,time : 1.7,position : 13,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.7,position : 6,duration : 1.5},
            {type : ObstacleType.Meteor,time : 1.9,position : 2,duration : 2},
            {type : ObstacleType.GroundStab,time : 2.3,position : 9,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2.3,position : 4,duration : 1.5},
            {type : ObstacleType.Meteor,time : 2.6,position : 3,duration : 2},
            {type : ObstacleType.GroundStab,time : 2.7,position : 1,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 2.7,position : 14,duration : 1.5},
            {type : ObstacleType.Bullet,time : 3,position : 15,duration : 1.4},
            {type : ObstacleType.Bullet,time : 3,position : 12,duration : 1.4}
        ]
    },
    {
        waveTime : 4.2,
        array : [
            {type : ObstacleType.Bullet,time : 0.5,position : 1,duration : 2.3},
            {type : ObstacleType.Bullet,time : 0.6,position : 6,duration : 2.2},
            {type : ObstacleType.GroundStab,time : 0.7,position : 6,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 0.7,position : 12,duration : 1.5},
            {type : ObstacleType.Bullet,time : 1.4,position : 9,duration : 3},
            {type : ObstacleType.GroundStab,time : 1.5,position : 3,duration : 1.5},
            {type : ObstacleType.Bullet,time : 1.9,position : 15,duration : 2.8},
            {type : ObstacleType.GroundStab,time : 2,position : 15,duration : 1.5},
            {type : ObstacleType.Bullet,time : 2.3,position : 7,duration : 3.3},
            {type : ObstacleType.Bullet,time : 2.5,position : 10,duration : 2.7},
            {type : ObstacleType.GroundStab,time : 2.8,position : 0,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 3.1,position : 9,duration : 1.5},
            {type : ObstacleType.Bullet,time : 3.3,position : 0,duration : 2.1},
            {type : ObstacleType.GroundStab,time : 3.5,position : 10,duration : 1.5},
            {type : ObstacleType.Bullet,time : 3.8,position : 5,duration : 1.8}
        ]
    },
    {
        waveTime : 3.2,
        array : [
            {type : ObstacleType.Bullet,time : 0.5,position : 13,duration : 2},
            {type : ObstacleType.Meteor,time : 0.7,position : 2,duration : 2},
            {type : ObstacleType.Bullet,time : 1,position : 2,duration : 3.1},
            {type : ObstacleType.Bullet,time : 1.1,position : 4,duration : 2.5},
            {type : ObstacleType.Meteor,time : 1.5,position : 8,duration : 2},
            {type : ObstacleType.Bullet,time : 1.6,position : 10,duration : 2.7},
            {type : ObstacleType.GroundStab,time : 1.9,position : 11,duration : 1.5},
            {type : ObstacleType.Bullet,time : 2.2,position : 5,duration : 2.7},
            {type : ObstacleType.Meteor,time : 2.3,position : 3,duration : 2},
            {type : ObstacleType.Bullet,time : 2.4,position : 0,duration : 2.2},
            {type : ObstacleType.Bullet,time : 2.5,position : 6,duration : 2.4}
        ]
    },
    {
        waveTime : 4.1,
        array : [
            {type : ObstacleType.Bullet,time : 0.5,position : 9,duration : 2.3},
            {type : ObstacleType.Bullet,time : 0.6,position : 14,duration : 2.2},
            {type : ObstacleType.GroundStab,time : 0.7,position : 9,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 0.7,position : 3,duration : 1.5},
            {type : ObstacleType.Bullet,time : 1.4,position : 1,duration : 3},
            {type : ObstacleType.GroundStab,time : 1.5,position : 12,duration : 1.5},
            {type : ObstacleType.Bullet,time : 1.9,position : 7,duration : 2.8},
            {type : ObstacleType.GroundStab,time : 2,position : 0,duration : 1.5},
            {type : ObstacleType.Bullet,time : 2.3,position : 15,duration : 3.3},
            {type : ObstacleType.Bullet,time : 2.5,position : 2,duration : 2.7},
            {type : ObstacleType.GroundStab,time : 2.8,position : 15,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 3.1,position : 6,duration : 1.5},
            {type : ObstacleType.Bullet,time : 3.3,position : 8,duration : 2.1},
            {type : ObstacleType.GroundStab,time : 3.5,position : 5,duration : 1.5},
            {type : ObstacleType.Bullet,time : 3.8,position : 13,duration : 1.8}
        ]
    },
    //44
    {
        waveTime : 4,
        array : [
            {type : ObstacleType.Bullet,time : 0.5,position : 12,duration : 2},
            {type : ObstacleType.Laser,time : 1.1,position : 1,duration : 1},
            {type : ObstacleType.Bullet,time : 1.4,position : 4,duration : 2.4},
            {type : ObstacleType.Laser,time : 1.8,position : 7,duration : 1},
            {type : ObstacleType.Meteor,time : 2,position : 2,duration : 2},
            {type : ObstacleType.Laser,time : 2.2,position : 4,duration : 0.8},
            {type : ObstacleType.Laser,time : 2.2,position : 2,duration : 0.7},
            {type : ObstacleType.Meteor,time : 2.3,position : 6,duration : 2},
            {type : ObstacleType.Laser,time : 2.8,position : 0,duration : 1},
            {type : ObstacleType.GroundStab,time : 3.1,position : 3,duration : 1.5},
            {type : ObstacleType.Laser,time : 3.2,position : 6,duration : 0.8},
            {type : ObstacleType.GroundStab,time : 3.3,position : 6,duration : 1.5}
        ]
    },
    {
        waveTime : 3.9,
        array : [
            {type : ObstacleType.Bullet,time : 0.5,position : 4,duration : 2},
            {type : ObstacleType.Laser,time : 1.1,position : 9,duration : 1},
            {type : ObstacleType.Bullet,time : 1.4,position : 12,duration : 2.4},
            {type : ObstacleType.Laser,time : 1.8,position : 15,duration : 1},
            {type : ObstacleType.Meteor,time : 2,position : 6,duration : 2},
            {type : ObstacleType.Laser,time : 2.2,position : 12,duration : 1},
            {type : ObstacleType.Laser,time : 2.2,position : 10,duration : 1},
            {type : ObstacleType.Meteor,time : 2.3,position : 2,duration : 2},
            {type : ObstacleType.Laser,time : 2.8,position : 8,duration : 1},
            {type : ObstacleType.GroundStab,time : 3.1,position : 12,duration : 1.5},
            {type : ObstacleType.Laser,time : 3.2,position : 14,duration : 1},
            {type : ObstacleType.GroundStab,time : 3.3,position : 9,duration : 1.5}
        ]
    },
    {
        waveTime : 4.2,
        array : [
            {type : ObstacleType.Bullet,time : 0.5,position : 1,duration : 2.3},
            {type : ObstacleType.Bullet,time : 0.6,position : 6,duration : 2.2},
            {type : ObstacleType.GroundStab,time : 0.7,position : 6,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 0.7,position : 12,duration : 1.5},
            {type : ObstacleType.Bullet,time : 1.4,position : 9,duration : 3},
            {type : ObstacleType.GroundStab,time : 1.5,position : 3,duration : 1.5},
            {type : ObstacleType.Bullet,time : 1.9,position : 15,duration : 2.8},
            {type : ObstacleType.GroundStab,time : 2,position : 15,duration : 1.5},
            {type : ObstacleType.Bullet,time : 2.3,position : 7,duration : 3.3},
            {type : ObstacleType.Bullet,time : 2.5,position : 10,duration : 2.7},
            {type : ObstacleType.GroundStab,time : 2.8,position : 0,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 3.1,position : 9,duration : 1.5},
            {type : ObstacleType.Bullet,time : 3.3,position : 0,duration : 2.1},
            {type : ObstacleType.GroundStab,time : 3.5,position : 10,duration : 1.5},
            {type : ObstacleType.Bullet,time : 3.8,position : 5,duration : 1.8}
        ]
    },
    //48
    {
        waveTime : 2.6,
        array : [
            {type : ObstacleType.GroundStab,time : 0.5,position : 15,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 0.5,position : 6,duration : 1.5},
            {type : ObstacleType.Meteor,time :0.7,position : 6,duration : 2},
            {type : ObstacleType.GroundStab,time : 1.4,position : 4,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.4,position : 7,duration : 1.5},
            {type : ObstacleType.Meteor,time : 1.6,position : 8,duration : 2},
            {type : ObstacleType.GroundStab,time : 1.7,position : 1,duration : 1.5},
            {type : ObstacleType.GroundStab,time : 1.9,position : 4,duration : 1.5},
            {type : ObstacleType.Meteor,time : 2.2,position : 4,duration : 2}
        ]
    },
    //49
    {
        waveTime : 2.6,
        array : [
            {type : ObstacleType.GroundStab,time : 0.5,position : 9,duration : 1.5},
            {type : ObstacleType.Laser,time : 0.7,position : 2,duration : 1},
            {type : ObstacleType.GroundStab,time : 0.9,position : 12,duration : 1.5},
            {type : ObstacleType.Meteor,time :1.2,position : 0,duration : 2},
            {type : ObstacleType.Laser,time : 1.8,position : 6,duration : 1},
            {type : ObstacleType.Meteor,time : 2,position : 8,duration : 2},
            {type : ObstacleType.Bullet,time : 2.1,position : 1,duration : 3},
            {type : ObstacleType.Bullet,time : 2.2,position : 13,duration : 3},
            {type : ObstacleType.Bullet,time : 2.3,position : 8,duration : 3},
            {type : ObstacleType.Bullet,time : 2.4,position : 0,duration : 3}
        ]
    },
    {
        waveTime : 2.6,
        array : [
            {type : ObstacleType.GroundStab,time : 0.5,position : 6,duration : 1.5},
            {type : ObstacleType.Laser,time : 0.7,position : 10,duration : 1},
            {type : ObstacleType.GroundStab,time : 0.9,position : 3,duration : 1.5},
            {type : ObstacleType.Meteor,time :1.2,position : 8,duration : 2},
            {type : ObstacleType.Laser,time : 1.8,position : 14,duration : 1},
            {type : ObstacleType.Meteor,time : 2,position : 0,duration : 2},
            {type : ObstacleType.Bullet,time : 2.1,position : 9,duration : 3},
            {type : ObstacleType.Bullet,time : 2.2,position : 5,duration : 3},
            {type : ObstacleType.Bullet,time : 2.3,position : 0,duration : 3},
            {type : ObstacleType.Bullet,time : 2.4,position : 8,duration : 3}
        ]
    },
    //51
    {}
];

var IS_TUTORED = false;
var REWARD_BULLET_LENGTH = 55;
var REWARD_BULLET_WIDTH = 55;
var REWARD_LASER = 59;
var REWARD_GROUNDSTAB = 59;
var REWARD_COIN = 10;
var MAP_POINT = cc.p(400,300);
var BEST_SPRITE_MAIN = cc.p(90,550);
var SCORE_SPRITE_MAIN = cc.p(90,500);
var BEST_LABEL_MAIN = cc.p(220,552);
var BEST_LABEL_OVER = cc.p(460,290);
var SCORE_LABEL_MAIN = cc.p(220,500);
var SCORE_LABEL_OVER = cc.p(400,367);
var SCOREADD_LABEL = cc.p(209,450);
var OPERATION_SPRITE = cc.p(126,96);
var LOGIN_BUTTON = cc.p(755,560);
var MAINMENU_MAIN = cc.p(614,573);
var MAIN_MENU_OVER = cc.p(280,180);
var MUTE_SPRITE = cc.p(656,573);
var SHARE_MAIN = cc.p(698,573);
var SHARE_OVER = cc.p(520,180);
var RESTART_BUTTON = cc.p(400,180);
var IONIC_LABEL = cc.p(400,100);
var REWARD_POS = cc.p(900,450);
var REWARD_DIS = 200;
var FONT_SIZE = 30;
var OVER_PIC_POINT = cc.p(400,350);
var PLAYMUSIC = false;
var PLAYER_FINAL_SCALE = 1;

/** @expose */
var IONIC_SENTENCES = [
    "开发者妹子的QQ和你没什么关系。",
    "手残者剁手即愈，脑残者无药医也",
    "画面太美我不敢看(/▽╲)",
    "就你这水准，扫马桶还是可以勉强胜任的",
    "看电视去吧，这游戏你玩不来。",
    "不要拉低国民智商水平",
    "策划君说黑猩猩都能玩得比你高",
    "禁止猪类玩本游戏！",
    "打得不错，你就这么安慰自己吧<(￣ˇ￣)/",
    "且行且珍惜",
    "嗯，蛮拼的",
    "我和你认真，你却和我开玩笑",
    "没有青铜六这个段位吗？！好吧那你没段位了。",
    "嗯，渐入佳境。",
    "这个成绩还可以算得上及格",
    "标准星人，我们去抓水母吧",
    "别太高兴，你离不及格也没多远",
    "活捉一只高玩！",
    "好好嘲讽下那些手残党吧",
    "你的成绩已经和开发者不相上下了！",
    "遭遇神级玩家！",
    "继续努力，开发者妹子的QQ号等着你！",
    "天梯前十名的大神"
];

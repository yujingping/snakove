var res = {
    Bullet_png : "res/textures/bullet.png",
    GroundStab_warningPng : "res/textures/groundStab_warning.png",
    GroundStab_warningPlist : "res/textures/groundStab_warning.plist",
    GroundStab_operatingPng : "res/textures/groundStab_operating.png",
    GroundStab_operatingPlist : "res/textures/groundStab_operating.plist",
    Laser_warningPlist : "res/textures/laser_warning.plist",
    Laser_warningPng : "res/textures/laser_warning.png",
    UILayer_Computer : "res/GameSceneComputer.json",
    UIGameOver_Computer : "res/GameOverViewComputer.json",
    Laser_OperatingPlist : "res/textures/laser_operating.plist",
    Laser_OperatingPng : "res/textures/laser_operating.png",
    Meteorite_warningPlist : "res/textures/meteorite_warning.plist",
    Meteorite_warningPng : "res/textures/meteorite_warning.png",
    Meteorite_operatingPlist : "res/textures/meteorite_warning.plist",
    Meteorite_operatingPng : "res/textures/meteorite_warning.png",
    Player_front : "res/textures/player front.png",
    Player_die : "res/textures/playerDie.png",
    Coin_plist : "res/textures/coin.plist",
    Coin_png : "res/textures/coin.png",
    UI_plist : "res/textures/ui.plist",
    UI_png : "res/textures/ui.png",
    MainPic : "res/textures/mainBackground.png",
    Start_btn : "res/textures/start1.png",
    Start_btn_p : "res/textures/start2.png",
    map_png : "res/textures/map.png",
    reward1 : "res/textures/achievement1.png",
    reward2 : "res/textures/achievement2.png",
    reward3 : "res/textures/achievement3.png",
    reward4 : "res/textures/achievement4.png",
    OverPic : "res/textures/gameover.png",
    gameScoreFnt : 'res/textures/gamescore.fnt',
    gameOverScoreFnt : "res/textures/gamescore60.fnt",
    gameOverBestScoreFnt : "res/textures/bestscore.fnt",
    Chinese : "res/textures/Chinese.fnt",
    ChinesePng : "res/textures/Chinese.png",
    rewardFX : "res/FX/playerCredits.plist",
    deadFX : "res/FX/playerDead.plist",
    coinFX : "res/FX/coin.plist",
    FX3 : "res/FX/3.plist",
    FX4 : "res/FX/4.plist",
    dieFX : "res/FX/playerDead.plist",
    stabT : "res/textures/stepT.png",
    bulletT : "res/textures/bulletT.png",
    meteT : "res/textures/meteT.png",
    laserT : "res/textures/laserT.png",
    gameMp3 : 'res/sounds/bgm2.mp3',
    readyPng : "res/textures/ready.png",
    goPng : "res/textures/go.png",
    coinMp3 : "res/sounds/coin.mp3",
    dieMp3 : "res/sounds/die.mp3",
    buttonMp3 : "res/sounds/button.mp3",
    meteMp3 : "res/sounds/mete.mp3",
    laserMp3 : "res/sounds/laser.mp3",
    greatMp3 : "res/sounds/great.mp3",
    iron : [
        "res/sounds/7.mp3",
        "res/sounds/9.mp3",
        "res/sounds/8.mp3",
        "res/sounds/5.mp3",
        "res/sounds/4.mp3",
        "res/sounds/1.mp3",
        "res/sounds/3.mp3",
        "res/sounds/2.mp3",
        "res/sounds/6.mp3"
    ]
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}

var ObstacleManager = cc.Node.extend({
    bulletAndStab : new Array(),
    laser : new Array(),
    meteor : new Array(),
    currentTime : -3,
    ctor : function(){
        this._super();
        this.Initialize();
    },
    Initialize : function(){
        this.bulletAndStab.length = 0;
        this.laser.length = 0;
        this.meteor.length = 0;
        this.currentTime = -3;
    },
    Update : function(dt){
        this.currentTime += dt;
        this.laser.sort(this.comp);
        this.meteor.sort(this.comp);
        this.bulletAndStab.sort(this.comp);
        while(this.laser.length != 0 && this.laser[0].spawnTime + this.laser[0].duration <= this.currentTime)
            this.laser.pop();
        while(this.bulletAndStab.length != 0 && this.bulletAndStab[0].spawnTime + this.bulletAndStab[0].duration <= this.currentTime)
            this.bulletAndStab.pop();
        while(this.meteor.length != 0 && this.meteor[0].spawnTime + this.meteor[0].duration <= this.currentTime)
            this.meteor.pop();
    },
    comp : function(a,b){
        return (a.spawnTime + a.duration) > (b.spawnTime + b.duration);
    },
    InsertObstacle : function(type,obstacle){
        switch (type){
            case ObstacleType.Bullet :
                this.bulletAndStab.unshift(obstacle);
                break;
            case ObstacleType.Laser :
                this.laser.unshift(obstacle);
                break;
            case ObstacleType.Meteor :
                this.meteor.unshift(obstacle);
                break;
            case ObstacleType.GroundStab :
                this.bulletAndStab.unshift(obstacle);
                break;
            default : break;
        }
    },
    RemoveAllObstacles : function(){
        while(this.bulletAndStab.length != 0){
            this.bulletAndStab[0].removeFromParent(true);
            this.bulletAndStab.pop();
        }
        while(this.laser.length != 0){
            this.laser[0].removeFromParent(true);
            this.laser.pop();
        }
        while(this.meteor.length != 0){
            this.meteor[0].removeFromParent(true);
            this.meteor.pop();
        }
    }
});

var RewardManager = cc.Node.extend({
    rewardStack : new Array(),
    currentNum : 0,
    totNum : 0,
    isFull : true,
    sprite : null,
    scoreManager : null,
    ctor : function(scoreManager){
        this._super();
        this.scoreManager = scoreManager;
        if(!ENVIRONMENT)this.runAction(cc.moveBy(0.01,cc.p(0,100)));
        this.Initialize();
    },
    Initialize : function(){
        this.currentNum = this.totNum = 0;
        if(this.sprite != null)
        this.sprite.removeFromParent();
        this.sprite = null;
        this.isFull = true;
        this.sprite = new cc.Sprite();
        this.addChild(this.sprite,1);
        if(!ENVIRONMENT){
            this.sprite.scaleX = this.sprite.scaleY = 1.5;
        }
    },
    AddReward : function(num,score){
        this.rewardStack.unshift(num);
        this.totNum++;
        if(this.isFull)
            this.RewardPushing();
        this.scoreManager.AddScore(Math.floor(score),true);
    },
    RewardPushing : function(){
        if(true){
            if(this.currentNum == this.totNum){
                this.isFull = true;
                return;
            }
            this.isFull = false;
            this.currentNum++;
            var num = this.rewardStack.shift();
            switch (num){
                case 1 :
                    this.sprite.initWithFile(res.reward1);
                    break;
                case 2 :
                    this.sprite.initWithFile(res.reward3);
                    break;
                case 3 :
                    this.sprite.initWithFile(res.reward2);
                    break;
                case 4 :
                    this.sprite.initWithFile(res.reward4);
                    break;
                default : break;
            }
            this.sprite.attr({
                x : REWARD_POS.x,
                y : REWARD_POS.y
            });
            var that = this;
            this.sprite.runAction(cc.sequence(cc.moveBy(0.25,cc.p(-REWARD_DIS,0)),cc.callFunc(function(){ParticleManager(that,cc.p(REWARD_POS.x - REWARD_DIS,REWARD_POS.y),2,res.FX4);AudioManager(res.greatMp3);}),cc.delayTime(0.5),cc.fadeOut(0.25),cc.moveBy(0.01,cc.p(REWARD_DIS,0)),cc.fadeIn(0.01),cc.callFunc(function(){that.RewardPushing();})));
        }
    }
});

var ScoreManager = cc.Node.extend({
    currentScore : 0,
    bestScore : 0,
    gamingSceneUI : null,
    currentScoreLabelToBeControlled : null,
    currentAddScoreLabelToBeControlled : null,
    score_temporary : 0,
    isAllowedToAdd : true,
    multiplier : 0,
    waveController : null,
    rating : 0,
    ctor : function(UI,waveController){
        this._super();
        this.gamingSceneUI = UI;
        this.currentScoreLabelToBeControlled = this.gamingSceneUI.scoreLabel;
        this.currentAddScoreLabelToBeControlled = this.gamingSceneUI.scoreAddLabel;
        this.waveController = waveController;
        this.Initialize();
    },
    Initialize : function(){
        this.currentScore = 0;
        this.bestScore = 0;
        this.score_temporary = 0;
        this.isAllowedToAdd = true;
        this.multiplier = 1;
        this.rating = 0;
        this.scheduleUpdate(this.update);
    },
    update : function(dt){
        var difference = this.currentScore - this.score_temporary;
        var step = 19;//this value is to be changed later!
        step = Math.min(difference,Math.max(step,difference / 19));
        step = Math.floor(step);
        var num = parseInt(this.waveController.GetCoefficient() + Math.random() * 0.2 - 0.1);
        this.AddScore(num,false);
        this.currentScoreLabelToBeControlled.setString(this.score_temporary + step);
        this.score_temporary += step;
    },
    AddScore : function(deltaScore,display){//deltaScore is to be set allergic to a certain big value
        if(!this.isAllowedToAdd)return;
        deltaScore = Math.floor(deltaScore * this.waveController.GetCoefficient());
        this.currentScore += deltaScore;
        var that = this.currentAddScoreLabelToBeControlled;
        if(deltaScore >= 10 && display){
            ParticleManager(this,cc.p(this.currentAddScoreLabelToBeControlled.x,this.currentAddScoreLabelToBeControlled.y),1,res.rewardFX);
            this.currentAddScoreLabelToBeControlled.runAction(cc.sequence(
                cc.callFunc(function(){
                    that.setString("+" + deltaScore);
                }),
                cc.delayTime(1),
                cc.callFunc(function(){
                    that.setString("");
                })
            ));
        }
    },
    GetBestScore : function(){
        this.isAllowedToAdd = false;
        if(this.currentScore > cc.sys.localStorage.getItem("bestScore")){
            /** @expose */
            yw.openSocialShareMenu({
                "text" : "我在这个游戏里获得了10000分！快来挑战我吧！",
                "title" : "#严禁智障患者玩此游戏!#我在这个游戏中获得了" + (Math.random() * 10900 + 4500) + "分，快点来挑战我啊！",
            },function(){});
            cc.sys.localStorage.setItem("bestScore",this.currentScore);
        }
        this.bestScore = cc.sys.localStorage.getItem("bestScore");
        this.unscheduleUpdate();
        return this.bestScore;
    }
});

var ParticleManager = function(target,position,duration,FX){
    if(!PAR)return;
    var p = new cc.ParticleSystem(FX);
    p.attr({
        x : position.x,
        y : position.y
    });
    if(!ENVIRONMENT){
        p.scaleX /= 2;
        p.scaleY /= 2;
    }
    target.addChild(p);
    p.runAction(cc.sequence(
        cc.delayTime(duration),
        cc.callFunc(function(){
            p.removeFromParent(true);
        })
    ));
};

var AudioManager = function(audio){
    if(!ENABLE_AUDIO)return;
    cc.audioEngine.playEffect(audio,false);
};
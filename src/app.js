var PlayerTesterScene = cc.Scene.extend({
    playerLayer : null,
    playerSprite : null,
    initializer : null,
    obstacleLayer : null,
    gamingUILayer : null,
    collisionDetector : null,
    rewardObject : null,
    rewardManager : null,
    scoreManager : null,
    isPlayerDead : false,
    isAdd : false,
    onEnter:function () {
        this._super();
        var colorLayer = new cc.LayerColor(cc.color(156, 211, 213, 255), SCENE_WIDTH * 2, SCENE_HEIGHT * 2);
        colorLayer.ignoreAnchorPointForPosition(false);
        this.addChild(colorLayer);
        this.isPlayerDead = false;
        this.initializer = new Initializer();
        this.initializer.InitAnimation();
        this.obstacleLayer = new ObstacleLayer();
        this.addChild(this.obstacleLayer,1);
        this.playerLayer = new PlayerLayer();
        this.playerSprite = this.playerLayer.player;
        this.addChild(this.playerLayer,0);
        this.collisionDetector = new CollisionDetector(this.playerSprite,this.obstacleLayer);
        this.gamingUILayer = new GamingSceneUILayer();
        this.addChild(this.gamingUILayer,100);
        this.rewardObject = new RewardObject();
        this.scoreManager = new ScoreManager(this.gamingUILayer.gamingSceneUI,this.obstacleLayer.obstacleWaveController);
        this.addChild(this.scoreManager);
        this.rewardManager = new RewardManager(this.scoreManager);
        this.addChild(this.rewardManager);
        this.Initialize();
        if(!ENVIRONMENT){
            this.obstacleLayer.runAction(cc.moveBy(0.01,cc.p(0,50)));
            this.rewardObject.runAction(cc.moveBy(0.01,cc.p(0,50)));
            this.playerLayer.runAction(cc.moveBy(0.01,cc.p(0,50)));
        }
    },
    update : function(dt){
        if(this.isPlayerDead)return;
        this.collisionDetector.CheckCollision(this);
        if(this.collisionDetector.CheckGetReward(this.rewardObject,this.playerSprite))
            this.scoreManager.AddScore(200,true);
        if(IS_TUTORED && !this.isAdd){
            this.addChild(this.rewardObject);
            this.isAdd = true;
            console.log("Added");
        }
    },
    PlayerDie : function() {
        var that = this;
        AudioManager(res.dieMp3);
        this.unscheduleUpdate();
        this.isPlayerDead = true;
        this.playerLayer.GameOver();
        this.gamingUILayer.runAction(cc.sequence(
                cc.delayTime(1),
                cc.callFunc(function(){
                    that.gamingUILayer.GameOver(that.scoreManager.currentScore,that.scoreManager.GetBestScore(),that,that.obstacleLayer.obstacleWaveController.waveNum);
                })
            )
        );
        this.obstacleLayer.GameOver();
        this.rewardObject.removeAllChildren(true);
        this.rewardObject.Instantiate();
    },
    PlayerReward : function(num,distance){
        if(num == 1 || num == 2 || num == 4)
        {
            this.rewardManager.AddReward(num,parseInt(this.obstacleLayer.obstacleWaveController.GetCoefficient() * Math.pow(50 * distance / (48) + 50,1.5)));
        }
        else this.rewardManager.AddReward(num,parseInt(Math.pow(130 * distance / 27,1.5)));
    },
    Initialize : function(){
        this.scheduleUpdate();
        this.isPlayerDead = false;
        this.obstacleLayer.Initialize();
        this.playerLayer.Initialize();
        this.playerSprite = this.playerLayer.player;
        this.collisionDetector.SetPlayer(this.playerSprite);
        this.gamingUILayer.Initialize();
        this.rewardManager.Initialize();
        this.scoreManager.Initialize();
    }
});


var RewardObject = cc.Node.extend({
    sprite : null,
    ctor : function(){
        this._super();
        this.sprite = new cc.Sprite("#coin1.png");
        this.sprite.scale = 1.0;
        if(!ENVIRONMENT)this.sprite.setScale(1.5);
        this.Instantiate();
    },
    Instantiate : function(){
        var positionIndex = parseInt(Math.random() * 16);
        this.addChild(this.sprite,1);
        this.sprite.stopAllActions();
        this.sprite.runAction(cc.place(cc.p(-110,-110)));
        ParticleManager(this,cc.p(this.sprite.x,this.sprite.y),1,res.FX3);
        this.sprite.runAction(cc.sequence(cc.place(cc.p(-110,-110)),cc.delayTime(0.3),cc.place(StabPos[positionIndex])));
        this.sprite.runAction(cc.animate(AnimateController(Animations.Coin,0.15,false)).repeatForever());
    }
});

var Notice = cc.Node.extend({
    sprite : null,
    scalee : 1,
    ctor : function(type) {
        this._super();
        this.scalee = 1;
        switch (type) {
            case 1 :
                this.sprite = new cc.Sprite(res.bulletT);
                break;
            case 2 :
                this.sprite = new cc.Sprite(res.laserT);
                break;
            case 3 :
                this.sprite = new cc.Sprite(res.stabT);
                break;
            case 4 :
                this.sprite = new cc.Sprite(res.meteT);
                break;
            case 5 :
                this.sprite = new cc.Sprite(res.readyPng);
                this.scalee = 0.5;
                break;
            case 6 :
                this.sprite = new cc.Sprite(res.goPng);
                this.scalee = 0.5;
                break;
            default : break;
        }
        this.attr({
            x: MAP_POINT.x,
            y: MAP_POINT.y,
            anchorX: 0.5,
            anchorY: 0.5
        });
        this.sprite.scaleX = this.sprite.scaleY = 0;
        this.addChild(this.sprite);
        var that = this;
        this.sprite.runAction(cc.sequence(
            cc.spawn(
                cc.rotateBy(0.5, 720),
                cc.scaleTo(0.5, that.scalee)
            ),
            cc.delayTime(1.7),
            cc.fadeOut(0.5),
            cc.callFunc(function () {
                that.removeFromParent(true);
            })
        ));
    }
});
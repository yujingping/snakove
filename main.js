/**
 * A brief explanation for "project.json":
 * Here is the content of project.json file, this is the global configuration for your game, you can modify it to customize some behavior.
 * The detail of each field is under it.
 {
    "project_type": "javascript",
    // "project_type" indicate the program language of your project, you can ignore this field

    "debugMode"     : 1,
    // "debugMode" possible values :
    //      0 - No message will be printed.
    //      1 - cc.error, cc.assert, cc.warn, cc.log will print in console.
    //      2 - cc.error, cc.assert, cc.warn will print in console.
    //      3 - cc.error, cc.assert will print in console.
    //      4 - cc.error, cc.assert, cc.warn, cc.log will print on canvas, available only on web.
    //      5 - cc.error, cc.assert, cc.warn will print on canvas, available only on web.
    //      6 - cc.error, cc.assert will print on canvas, available only on web.

    "showFPS"       : true,
    // Left bottom corner fps information will show when "showFPS" equals true, otherwise it will be hide.

    "frameRate"     : 60,
    // "frameRate" set the wanted frame rate for your game, but the real fps depends on your game implementation and the running environment.

    "id"            : "gameCanvas",
    // "gameCanvas" sets the id of your canvas element on the web page, it's useful only on web.

    "renderMode"    : 0,
    // "renderMode" sets the renderer type, only useful on web :
    //      0 - Automatically chosen by engine
    //      1 - Forced to use canvas renderer
    //      2 - Forced to use WebGL renderer, but this will be ignored on mobile browsers

    "engineDir"     : "frameworks/cocos2d-html5/",
    // In debug mode, if you use the whole engine to develop your game, you should specify its relative path with "engineDir",
    // but if you are using a single engine file, you can ignore it.

    "modules"       : ["cocos2d"],
    // "modules" defines which modules you will need in your game, it's useful only on web,
    // using this can greatly reduce your game's resource size, and the cocos console tool can package your game with only the modules you set.
    // For details about modules definitions, you can refer to "../../frameworks/cocos2d-html5/modulesConfig.json".

    "jsList"        : [
    ]
    // "jsList" sets the list of js files in your game.
 }
 *
 */

var ENVIRONMENT;
var PAR;

var ConstantsInitializer = function(environment){
    ENVIRONMENT = environment;
    PLAYMUSIC = ENVIRONMENT;
    /** @expose */
    yw.init({
        //gameId: '10002325',
        gameId : '10002671',
        barrageEnabled: ENVIRONMENT,
        rankEnabled: true
    });
    if(environment){
        SCENE_HEIGHT = 600;
        SCENE_WIDTH = 800;
        DISPLAY_POLICY = cc.ResolutionPolicy.SHOW_ALL;
    }
    else{
        PLAYER_STEP_HALF = 15 * 1.5;
        SCENE_HEIGHT = 500;
        SCENE_WIDTH = 300;
        BULLET_HALF_LENGTH *= 0.75;
        DISPLAY_POLICY = cc.ResolutionPolicy.SHOW_ALL;
        BulletPos = [
            cc.p(82.5,400 + BULLET_HALF_LENGTH / 2),
            cc.p(127.5,400 + BULLET_HALF_LENGTH / 2),
            cc.p(172.5,400 + BULLET_HALF_LENGTH / 2),
            cc.p(217.5,400 + BULLET_HALF_LENGTH / 2),
            cc.p(300 + BULLET_HALF_LENGTH / 2,267.5),
            cc.p(300 + BULLET_HALF_LENGTH / 2,222.5),
            cc.p(300 + BULLET_HALF_LENGTH / 2,177.5),
            cc.p(300 + BULLET_HALF_LENGTH / 2,132.5),
            cc.p(217.5,-BULLET_HALF_LENGTH / 2),
            cc.p(172.5,-BULLET_HALF_LENGTH / 2),
            cc.p(127.5,-BULLET_HALF_LENGTH / 2),
            cc.p(82.5,-BULLET_HALF_LENGTH / 2),
            cc.p(-50,132.5),
            cc.p(-50,177.5),
            cc.p(-50,222.5),
            cc.p(-50,267.5)
        ];
        StabPos = [
            cc.p(82.5,267.5),
            cc.p(127.5,267.5),
            cc.p(172.5,267.5),
            cc.p(217.5,267.5),
            cc.p(82.5,222.5),
            cc.p(127.5,222.5),
            cc.p(172.5,222.5),
            cc.p(217.5,222.5),
            cc.p(82.5,177.5),
            cc.p(127.5,177.5),
            cc.p(172.5,177.5),
            cc.p(217.5,177.5),
            cc.p(82.5,132.5),
            cc.p(127.5,132.5),
            cc.p(172.5,132.5),
            cc.p(217.5,132.5)
        ];
        LaserPos = [
            cc.p(82.5,312.5),
            cc.p(127.5,312.5),
            cc.p(172.5,312.5),
            cc.p(217.5,312.5),
            cc.p(272.5,267.5),
            cc.p(272.5,222.5),
            cc.p(272.5,177.5),
            cc.p(272.5,132.5),
            cc.p(217.5,87.5),
            cc.p(172.5,87.5),
            cc.p(127.5,87.5),
            cc.p(82.5,87.5),
            cc.p(37.5,132.5),
            cc.p(37.5,177.5),
            cc.p(37.5,222.5),
            cc.p(37.5,267.5)
        ];
        MeteorPos = [
            cc.p(105,200),
            cc.p(150,200),
            cc.p(195,200),
            cc.p(105,195),
            cc.p(150,195),
            cc.p(195,195),
            cc.p(105,150),
            cc.p(150,150),
            cc.p(195,150)
        ];
        REWARD_BULLET_LENGTH = 27.5 * 1.5;
        REWARD_BULLET_WIDTH = 27.5 * 1.5;
        REWARD_LASER = 29.5 * 1.5;
        REWARD_GROUNDSTAB = 29.5 * 1.5;
        REWARD_COIN = 5 * 1.5;
        PLAYER_SCALE = 28 * 1.5;
        BULLET_LENGTH = 20 * 1.5;
        BULLET_WIDTH = 7.5 * 1.5;
        GROUND_STAB_HALF = 15;
        PLAYER_FINAL_SCALE = 1.5;
    }
    IS_TUTORED = false;
};

function UICertain(){
    if(ENVIRONMENT)return;
    var size = cc.director.getWinSize();
    var height = size.height;
    BEST_SPRITE_MAIN = cc.p(47.5,height - 39 / 2);
    BEST_LABEL_MAIN = cc.p(62,height - 81 / 2);
    SCORE_SPRITE_MAIN = cc.p(47.5,height - 129 / 2);
    SCORE_LABEL_MAIN = cc.p(62,height - 174 / 2);
    SCOREADD_LABEL = cc.p(62,height - 216 / 2);
    LOGIN_BUTTON = cc.p(280.5,height - 33 / 2);
    MAINMENU_MAIN = cc.p(210,height - 33 / 2);
    MUTE_SPRITE = cc.p(231,height - 33 / 2);
    SHARE_MAIN = cc.p(252,height - 33 / 2);
    OPERATION_SPRITE = cc.p(60,50);
    FONT_SIZE /= 2;
    BEST_LABEL_OVER = cc.p(225,110);
    SCORE_LABEL_OVER = cc.p(200,150);
    MAIN_MENU_OVER = cc.p(125,65);
    SHARE_OVER = cc.p(275,65);
    IONIC_LABEL = cc.p(200,30);
    RESTART_BUTTON = cc.p(200,65);
    MAP_POINT = cc.p(150,200);
    REWARD_POS = cc.p(350,300);
    REWARD_DIS = 100;
    OVER_PIC_POINT = cc.p(200,140);
}

cc.game.onStart = function(){
    if(!cc.sys.isNative && document.getElementById("cocosLoading")) //If referenced loading.js, please remove it
        document.body.removeChild(document.getElementById("cocosLoading"));

    // Pass true to enable retina display, disabled by default to improve performance
    cc.view.enableRetina(false);
    // Adjust viewport meta
    cc.view.adjustViewPort(true);
    // Setup the resolution policy and design resolution size
    /** @expose */
    ConstantsInitializer(yw.hostEnv() === 'others');
    /** @expose */
    PAR = yw.hostEnv();
    /** @expose */
    PAR = (PAR == 'iPhone' || PAR == 'iPad' || PAR == 'iPod' || PAR == 'ios' || PAR == 'others') && (PAR != 'android') && (PAR != '5miao');
    cc.view.setDesignResolutionSize(SCENE_WIDTH,SCENE_HEIGHT,DISPLAY_POLICY);
    UICertain();
    // The game will be resized when browser size change
    cc.view.resizeWithBrowserSize(true);
    //load resources
    if(!ENVIRONMENT)
    cc.director.setContentScaleFactor(2);
    cc.LoaderScene.preload(g_resources, function () {
        //cc.director.runScene(new PlayerTesterScene());
        cc.director.runScene(new MainMenu())
    }, this);
};
cc.game.run();